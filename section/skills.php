<!-- Section des Compétences -->
<section id="skills" class="py-5 fade-up-on-scroll">
    <!-- Conteneur -->
    <div class="container">
        <!-- Titre de la section -->
        <header class="section-title">
            <h2>Compétences</h2>
            <!-- Description -->
            <p>
                Je suis un développeur engagé, possédant une expertise variée dans des domaines clés de la programmation et du développement Web. Mon éventail de compétences
                atteste de ma polyvalence et de mon engagement à exceller dans le secteur de la technologie.
            </p>
        </header>
        <!-- Liste des compétences -->
        <article class="skills-list">
            <!-- Langages de Programmation -->
            <div class="skill-category">
                <h2>Langages de programmation</h2>
                <!-- Élément de compétence : HTML -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-html.svg" alt="html">
                    <span>HTML</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="80" style="width: 80%">80%</div>
                    </div>
                </div>
                <!-- Élément de compétence : CSS -->
                <div class="skill-item mt-2">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-css.svg" alt="css">
                    <span>CSS</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="80" style="width: 80%">80%</div>
                    </div>
                </div>
                <!-- Élément de compétence : JavaScript -->
                <div class="skill-item mt-2">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-javascript.svg" alt="javascript">
                    <span>JavaScript</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="50" style="width: 50%">50%</div>
                    </div>
                </div>
                <!-- Élément de compétence : PHP -->
                <div class="skill-item mt-2">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-php.svg" style="height: 48px; width: 48px;" alt="php">
                    <span>PHP</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="50" style="width: 50%">50%</div>
                    </div>
                </div>
                <!-- Élément de compétence : Dart -->
                <div class="skill-item mt-2">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-dart.svg" alt="dart">
                    <span>Dart</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="40" style="width: 40%">40%</div>
                    </div>
                </div>
            </div>
            <!-- Frameworks Front-End -->
            <div class="skill-category">
                <h2>Frameworks front-End</h2>
                <!-- Élément de compétence : Bootstrap -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-bootstrap.svg" alt="bootstrap">
                    <span>Bootstrap</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="60" style="width: 60%">60%</div>
                    </div>
                </div>
            </div>
            <!-- Développement Mobile -->
            <div class="skill-category">
                <h2>Développement mobile</h2>
                <!-- Élément de compétence : Flutter -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-flutter.svg" alt="flutter">
                    <span>Flutter</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="40" style="width: 40%">40%</div>
                    </div>
                </div>
            </div>
            <!-- Base de données -->
            <div class="skill-category">
                <h2>Base de données</h2>
                <!-- Élément de compétence : Mysql -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-mysql.svg" alt="mysql">
                    <span>MySQL</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="80" style="width: 80%">80%</div>
                    </div>
                </div>
            </div>
            <!-- CMS -->
            <div class="skill-category">
                <h2>CMS</h2>
                <!-- Élément de compétence : Wordpress -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-wordpress.svg" alt="wordpress">
                    <span>WordPress</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="40" style="width: 40%">40%</div>
                    </div>
                </div>
            </div>
            <!-- Version Control / Collaboration -->
            <div class="skill-category">
                <h2>Version Contrôle / Collaboration</h2>
                <!-- Élément de compétence : GitLab -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-gitlab.svg" alt="gitlab">
                    <span>GitLab</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="60" style="width: 60%">60%</div>
                    </div>
                </div>
            </div>
            <!-- Outils de Test et de Débogage -->
            <div class="skill-category">
                <h2>Maquette</h2>
                <!-- Élément de compétence : Figma -->
                <div class="skill-item">
                    <!-- Icône et nom de la compétence -->
                    <img class="icone" src="/assets/img/icone/icons8-figma.svg" alt="figma">
                    <span>Figma</span>
                    <!-- Barre de progression -->
                    <div class="progress mt-1">
                        <div class="progress-bar" data-value="70" style="width: 70%">70%</div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>