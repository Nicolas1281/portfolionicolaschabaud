<!-- Section Portfolio -->
<section id="portfolio" class="py-5">
    <!-- Conteneur principal -->
    <div class="container">
        <!-- Titre et description de la section -->
        <div class="section-title fade-up-on-scroll">
            <h2>Portfolio</h2>
            <p>
                Explorez mes dernières réalisations et découvrez mon enthousiasme pour le développement Web et le design. Chaque projet reflète mon engagement profond,
                ma créativité sans limites et ma recherche constante de qualité. Parcourez cette section pour en apprendre davantage sur mes compétences et mon parcours
                professionnel.
            </p>
        </div>
        <div class="cv-container my-3 fade-up-on-scroll">
            <a href="/6- Annexe 6-1 - Tableau de synthèse - Epreuve E4 - BTS SIO 2023 (1).xlsx" target="_blank" class="cv-button text-center interactive-effect me-3">
                Voir mon tableau de synthèse
            </a>
            <a href="/Attestation de stage.pdf" target="_blank" class="cv-button text-center interactive-effect me-3">
                Voir mes attestations de stage 
            </a>
            <a href="/Retroplanning.pdf" target="_blank" class="cv-button text-center interactive-effect">
                Voir mon rétroplanning
            </a>
        </div>

        <div class="my-3 text-center fade-up-on-scroll">
            <img width="30" height="30" src="https://img.icons8.com/fluency/48/general-warning-sign.png" alt="general-warning-sign" />
            <b>Veuillez cliquer sur l'image de chaque projet pour afficher les détails. Ne cliquez pas sur les textes des projets, car cela n'ouvrira pas les détails.</b>
            <img width="30" height="30" src="https://img.icons8.com/fluency/48/general-warning-sign.png" alt="general-warning-sign" />
        </div>

        <!-- Filtres du portfolio -->
        <div class="portfolio-filter fade-up-on-scroll">
            <button data-filter="*" class="filter-button active">Tous</button>
            <button data-filter=".filter-personnel" class="filter-button">Personnel</button>
            <button data-filter=".filter-ecole" class="filter-button">École</button>
            <button data-filter=".filter-stage" class="filter-button">Stage</button>
            <button data-filter=".filter-frontend" class="filter-button">Frontend</button>
            <button data-filter=".filter-backend" class="filter-button">Backend</button>
            <button data-filter=".filter-web" class="filter-button">Web</button>
            <button data-filter=".filter-mobile" class="filter-button">Mobile</button>
        </div>

        <div class="row portfolio-items row my-5">
            <!---------------------------
                Portfolio Brother Burger 
            ------------------------------->
            <!-- Premier projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-1" data-toggle="modal" data-target="#project-modal-video-1">
                        <source src="/assets/video/brothersburger/brothersburger.mp4" type="video/mp4">
                        <source src="/assets/video/brothersburger/brothersburger.mov" type="video/mov">
                        <source src="/assets/video/brothersburger/brotherburger.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Brothers Burger</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-1">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators1" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators1" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators1" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/brother_burger/BrothersBurger.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/brother_burger/BrothersBurger1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators1" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators1" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: BrothersBurger</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/brothers-burger" target="_blank">https://3types.fr/projets/brothers-burger</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/brothersburger.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel de brotherburger </strong>: <a href="https://brothersburger31.fr/" target="_blank">https://brothersburger31.fr/</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            <strong>Brother Burger : une expérience culinaire authentique et locale</strong><br><br>
                                            Deux frères passionnés par la cuisine ont créé Brother Burger avec une mission claire : partager l'amour de la bonne
                                            nourriture tout en respectant les produits, les saisons et les producteurs locaux. Leur viande de bœuf de race Aubrac,
                                            élevée en Aveyron, est soigneusement sélectionnée pour offrir une qualité halal exceptionnelle. Le pain brioché est
                                            confectionné par un artisan boulanger local et livré chaque matin, garantissant fraîcheur et qualité. Avec une gamme de
                                            bières artisanales 100% toulousaines et un menu créatif, Brother Burger est plus qu'un simple food truck ; c'est une
                                            expérience gastronomique qui valorise le local, le frais et le fait maison. Situés à Fonsorbes, ils sont facilement
                                            accessibles et offrent également des services de privatisation pour des événements spéciaux.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration de brotherburger sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration de Brother Burger sur le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Brother Burger.
                                            </li>
                                            <li>
                                                <strong>Développement de fonctionnalités interactives :</strong> en plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et gestion de version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement front-end :</strong> ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des besoins des clients :</strong> ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques.
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation.
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-1">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Julien Neyret -->
            <!-- Deuxiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-2" data-toggle="modal" data-target="#project-modal-video-2">
                        <source src="/assets/video/julienneyret/julienneyret.mp4" type="video/mp4">
                        <source src="/assets/video/julienneyret/julienneyret.mov" type="video/mov">
                        <source src="/assets/video/julienneyret/julienneyret.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Julien Neyret</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>

            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-2">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <!-- images -->
                                    <div class="col-lg-8">
                                        <img src="assets/img/portfolio/julien_neyret/JulienNeyret.webp" class="d-block carousel-image" alt="Apps 1">
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: JulienNeyret</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/julien-neyret" target="_blank">https://3types.fr/projets/julien-neyret</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/julien-neyret.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel de Julien Neyret </strong>: <a href="https://www.julienneyret.com/" target="_blank">https://www.julienneyret.com/</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            Le site de Julien Neyret sert de vitrine pour ses compétences en tant que vidéaste et monteur freelance basé à Montpellier.
                                            Le site présente son portfolio, qui comprend une gamme de projets audiovisuels, de l'écriture à la post-production. Avec des
                                            réalisations notables dans des domaines variés comme les escape games, les tournois de jeux vidéo et les JRPG, le site met en
                                            avant son expertise en cadrage, montage, motion design et sound design. Il offre également un moyen facile de le contacter
                                            pour des collaborations futures.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration de Julien Neyret sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet Julien Neyret dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Julien Neyret.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-2">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Ion-x -->
            <!-- troisiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-3" data-toggle="modal" data-target="#project-modal-video-3">
                        <source src="/assets/video/ion-x/ion-x.mp4" type="video/mp4">
                        <source src="/assets/video/ion-x/ion-x.mov" type="video/mov">
                        <source src="/assets/video/ion-x/ion-x.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Ion-x</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>

            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-3" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-3">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators2" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators2" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators2" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/ion-x/Ion-x.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/ion-x/Ion-x1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/ion-x/Ion-x2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators2" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators2" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: Ion-x</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/ion-x" target="_blank">https://3types.fr/projets/ion-x</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/ion-x.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel d'Ion-x </strong>: <a href="https://ion-x.space/" target="_blank">https://ion-x.space/</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            ION-X est une entreprise innovante spécialisée dans la propulsion spatiale pour petits satellites. Leur site Web offre une vue
                                            complète de leur technologie brevetée d'électro-spray de liquide ionique, promettant de révolutionner la manière dont les
                                            satellites sont propulsés en orbite. Le site détaille le fonctionnement de la technologie, ses avantages uniques et les étapes
                                            de développement futur. Il s'adresse à un public varié, des passionnés de l'espace aux investisseurs potentiels, et vise à établir
                                            ION-X comme un leader dans le domaine de la propulsion spatiale.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration d'Ion-x sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet Ion-x dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Ion-x.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-3">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Ultra Bike France -->
            <!-- Quatriéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-4" data-toggle="modal" data-target="#project-modal-video-4">
                        <source src="/assets/video/ubf/ubf.mp4" type="video/mp4">
                        <source src="/assets/video/ubf/ubf.mov" type="video/mov">
                        <source src="/assets/video/ubf/ubf.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Ultra Bike France</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-4" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-4">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- images -->
                                        <div class="col-lg-8">
                                            <img src="assets/img/portfolio/ubf/UBF.webp" class="d-block carousel-image" alt="Apps 1">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="portfolio-info">
                                            <!-- Informations sur le projet -->
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: UBF</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/ubf.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel de Ultra bike France </strong>: <a href="https://www.ultrabikefrance.fr/" target="_blank">https://www.ultrabikefrance.fr/</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            Ultra Bike France est une entreprise qui organise des épreuves d'ultra-cyclisme pour les spécialistes, mais aussi pour les cyclistes
                                            recherchant une nouvelle expérience. Ils proposent une épreuve par département, un championnat pour vous mesurer aux autres coureurs,
                                            une même ville pour le départ et l'arrivée, et distances : 200, 300 et 500 km. Les parcours ont de 3 000 à 10 000 mètres de dénivelé
                                            positif et les participants doivent terminer les épreuves en moins de 17 heures pour 200 km, 24 heures pour 300 km et 34 heures pour
                                            500 km. Ils offrent également un camp de base avec un parcours à suivre avec la trace GPX et un tracker pour le live tracking avec
                                            fonction SOS.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration de UBF sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet Ultra Bike France dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Ultra Bike France.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-4">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Azimut -->
            <!-- Cinquiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-5" data-toggle="modal" data-target="#project-modal-video-5">
                        <source src="/assets/video/azimut/azimut.mp4" type="video/mp4">
                        <source src="/assets/video/azimut/azimut.mov" type="video/mov">
                        <source src="/assets/video/azimut/azimut.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Azimut</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-5" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-5">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators4" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="5" aria-label="Slide 6"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="6" aria-label="Slide 7"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="7" aria-label="Slide 8"></button>
                                                <button type="button" data-bs-target="#carouselIndicators4" data-bs-slide-to="8" aria-label="Slide 9"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/azimut/Azimut.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut5.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut6.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut7.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/azimut/Azimut8.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators4" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators4" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="portfolio-info">
                                            <!-- Informations sur le projet -->
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: Azimut</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/azimut" target="_blank">https://3types.fr/projets/azimut</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/azimut.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            Azimut est une marque française de prêt-à-porter dédiée au voyage. Le studio de design graphique toulousain 3Types a travaillé sur
                                            l'identité visuelle de la marque, la création des motifs des vêtements, le suivi de fabrication et d'impression, ainsi que la
                                            réalisation de la campagne de communication Ulule. Leur collaboration avec Azimut comprend le branding, le logotype, l'illustration,
                                            la direction artistique et l'identité visuelle. Tout au long de l'année, 3Types a dessiné, suivi la fabrication des vêtements et
                                            créé la campagne de communication de la marque.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration d'Azimut sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet Azimut dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Azimut.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-5">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio QB -->
            <!-- Siziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-6" data-toggle="modal" data-target="#project-modal-video-6">
                        <source src="/assets/video/QB/QB.mp4" type="video/mp4">
                        <source src="/assets/video/QB/QB.mov" type="video/mov">
                        <source src="/assets/video/QB/QB.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>QB</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-6" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-6">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators5" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                                <button type="button" data-bs-target="#carouselIndicators5" data-bs-slide-to="5" aria-label="Slide 6"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/qb/QB.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/qb/QB1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/qb/QB2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/qb/QB3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/qb/QB4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/qb/QB5.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators5" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators5" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: QB</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/biere-du-quercy" target="_blank">https://3types.fr/projets/biere-du-quercy</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/biere-du-quercy.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            La Brasserie Merchien, située dans le Quercy, produit de la bière artisanale brassée à la main. La marque Bière du Quercy propose
                                            six types de bières différentes, chacune avec son propre caractère et son ambiance. Le studio de design graphique toulousain 3Types a
                                            réalisé l'identité visuelle de la marque, y compris le logotype et les étiquettes des différentes bières. Les bières sont produites à
                                            partir d'ingrédients biologiques et la brasserie est une entreprise familiale créée en 1997 par les parents de Zakarie Meakean, qui
                                            brasse la bière à la main. Les clients peuvent trouver les bières de la Brasserie Merchien dans différents endroits, notamment sur
                                            le site Untappd.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : intégration de QB sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du portfolio :</strong> durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet Bière du Quercy dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour Bière du Quercy.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-6">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio L'intangible -->
            <!-- Septiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect ">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-7" data-toggle="modal" data-target="#project-modal-video-7">
                        <source src="/assets/video/lintangible/lintangible.mp4" type="video/mp4">
                        <source src="/assets/video/lintangible/lintangible.mov" type="video/mov">
                        <source src="/assets/video/lintangible/lintangible.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>L'intangible</h4>
                        <p>Conception Visuelle / HTML-CSS / Page Web / Site Web / Portfolio</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-7" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-7">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators6" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="5" aria-label="Slide 6"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="6" aria-label="Slide 7"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="7" aria-label="Slide 8"></button>
                                                <button type="button" data-bs-target="#carouselIndicators6" data-bs-slide-to="8" aria-label="Slide 9"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible5.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible6.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible7.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/lintangible/L'intangible8.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators6" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators6" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Web design</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: L'intangible</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de 3types </strong>: <a href="https://3types.fr/projets/lintangible" target="_blank">https://3types.fr/projets/lintangible</a></li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/lintangible.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel de l'intangible </strong>: <a href="https://lintangible.com/" target="_blank">https://lintangible.com/</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            L'Intangible est un restaurant gastronomique créatif situé dans le Tarn au château de la Bousquétarié.
                                            Il est tenu par deux jeunes chefs, Emma et Antoine, qui allient leurs savoir-faire pour proposer une cuisine inventive et émotive à
                                            base de produits locaux et de qualité. Le restaurant offre une expérience authentique et hors du temps. La direction artistique de
                                            l'identité visuelle, le logotype, la papeterie et la typographie ont été créés pour nourrir l'émotion des clients.
                                            Les chefs proposent un menu unique qui puise dans la richesse des saisons et du patrimoine, constitué en fonction des saisons
                                            et des produits des producteurs locaux. L'objectif de L'Intangible est de créer un moment délicieux que le client doit garder
                                            en mémoire toute sa vie, comme l'a dit Alain Ducasse.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet pendant le stage : Intégration de L'intangible sur le site officiel de 3types</h2>
                                        <ul>
                                            <li>
                                                <strong>Intégration du Portfolio :</strong> Durant mon stage chez 3types, j'ai eu l'opportunité de travailler sur
                                                l'intégration du projet L'Intangible dans le portfolio de l'entreprise. J'ai utilisé HTML5 et CSS pour créer des pages
                                                visuellement attrayantes qui mettent en valeur le travail réalisé par 3types pour L'Intangible.
                                            </li>
                                            <li>
                                                <strong>Développement de Fonctionnalités Interactives :</strong> En plus de l'intégration, j'ai également contribué à la
                                                conception de fonctionnalités interactives sur les pages du portfolio, améliorant ainsi l'expérience utilisateur et
                                                l'engagement.
                                            </li>
                                            <li>
                                                <strong>Collaboration et Gestion de Version :</strong> J'ai utilisé GitLab pour le suivi des versions et la collaboration
                                                avec l'équipe de développement, ce qui a permis un flux de travail efficace et bien organisé.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>HTML5 et CSS :</strong> J'ai renforcé mes compétences en HTML5 et CSS, ce qui m'a permis de créer des pages web
                                                esthétiquement plaisantes et fonctionnelles.</li>
                                            <li><strong>Développement Front-end :</strong> Ce projet m'a donné une expérience pratique du développement front-end, y
                                                compris la conception de fonctionnalités interactives.</li>
                                            <li><strong>Collaboration avec GitLab :</strong> J'ai acquis de l'expérience dans l'utilisation de GitLab pour la gestion de
                                                version et la collaboration en équipe.</li>
                                            <li><strong>Compréhension des Besoins Clients :</strong> Ce stage m'a également permis de mieux comprendre comment traduire
                                                les besoins des clients en solutions techniques efficaces.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-7">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Hugo Zely -->
            <!-- Huitiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <video autoplay loop muted class="img-fluid" id="project-video-8" data-toggle="modal" data-target="#project-modal-video-8">
                        <source src="/assets/video/hugozely/hugozely.mp4" type="video/mp4">
                        <source src="/assets/video/hugozely/hugozely.mov" type="video/mov">
                        <source src="/assets/video/hugozely/hugozely.webm" type="video/webm">
                        Votre navigateur ne prend pas en charge la lecture de vidéos.
                    </video>
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Hugo Zely</h4>
                        <p>Conformité légale / HTML-CSS / Section bilingue (français et anglais) / Site Web</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-video-8" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-video-8">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <!-- images -->
                                    <div class="col-lg-8">
                                        <img src="assets/img/portfolio/hugozely/hugozely.webp" class="d-block carousel-image" alt="Apps 1">
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: conformité légale et éthique</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Client </strong>: Hugo Zely</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Projet de hugozely en français </strong>: <a href="https://hugozely.fr/fr/legals" target="_blank">https://hugozely.fr/fr/legals</a></li>
                                                <li><strong>Projet de hugozely en anglais </strong>: <a href="https://hugozely.fr/en/legals" target="_blank">https://hugozely.fr/en/legals</a></li>
                                                <li><strong>Code source (FR) </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/Hugozelylegalsfr.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Code source (EN) </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/blob/test/Hugozelylegalsen.php?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                                <li><strong>Lien du site officiel de hugozely </strong>: <a href="https://hugozely.fr/fr/home" target="_blank">https://hugozely.fr/fr/home</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du client de l'entreprise</h2>
                                        <p>
                                            Le site de Hugo Zely est un site personnel et professionnel dédié à la photographie. En tant que membre de l'entreprise 3Types,
                                            Hugo prend des photos pour leurs projets et présente son portfolio sur son site web. Ses photographies, prises à travers le monde,
                                            sont partagées pour que les visiteurs puissent les apprécier. De plus, le site propose une boutique en ligne où les clients peuvent
                                            acheter ses photos.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet : Intégration mentions légales en français/anglais sur le site de Hugo Zely</h2>
                                        <p>Sur le site personnel et professionnel de Hugo Zely, j'ai pris en charge la création de la section des mentions légales, en français et
                                            en anglais, en utilisant HTML et CSS. Cette section importante du site garantit la conformité légale et la transparence pour les
                                            visiteurs. Mon travail a consisté à concevoir et à mettre en forme ces mentions légales pour qu'elles soient claires et informatives.
                                            En résumé, mon rôle a été essentiel pour assurer la conformité légale du site de Hugo Zely.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>Conception Web </strong> : Capacité à concevoir et structurer une section spécifique d'un site web, en l'occurrence la section des mentions légales.</li>
                                            <li><strong>Développement Front-end</strong> : Utilisation de HTML et CSS pour intégrer et mettre en forme la section des mentions légales.</li>
                                            <li><strong>Conformité légale</strong> : Compréhension de l'importance de la conformité légale sur un site web et mise en œuvre des mentions légales pour garantir cette conformité.</li>
                                            <li><strong>Multilinguisme</strong> : Capacité à travailler avec du contenu en plusieurs langues, en l'occurrence le français et l'anglais, et à assurer une traduction précise et appropriée des mentions légales.</li>
                                            <li><strong>Attention aux détails</strong> : Assurer que les mentions légales sont claires, précises et informatives pour les visiteurs.</li>
                                            <li><strong>Collaboration</strong> : Travailler en étroite collaboration avec le propriétaire du site (Hugo Zely) et éventuellement avec d'autres membres de l'équipe pour intégrer les mentions légales.</li>
                                            <li><strong>Gestion de projet</strong> : Capacité à gérer un aspect spécifique d'un projet, de la conception à la mise en œuvre, tout en respectant les délais et les exigences.</li>
                                        </ul>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à la valorisation de l’image de l’organisation sur les
                                                    médias numériques en tenant compte du cadre juridique et des
                                                    enjeux économiques
                                                </li>
                                                <li>
                                                    Référencer les services en ligne de l’organisation et mesurer leur
                                                    visibilité.
                                                </li>
                                                <li>
                                                    Participer à l’évolution d’un site Web exploitant les données de
                                                    l’organisation
                                                </li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-video-8">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Smoothee -->
            <!-- Neuviéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-backend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/smoothee/Smoothee1.webp" alt="" class="img-fluid" id="project-image-1" data-toggle="modal" data-target="#project-modal-1">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Smoothee</h4>
                        <p>Tests unitaires / PHP / Résolution de problèmes complexes / Tests et validation</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-1">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators7" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators7" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators7" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/smoothee/smoothee1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/smoothee/smoothee2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators7" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators7" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Test et Assurance Qualité</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/tree/test/Smoothee-testsunitaire?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            Design Graphique. De la conception d'identités visuelles percutantes à la création de sites web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet de l'entreprise</h2>
                                        <p>
                                            Smoothee est un projet développé par l'entreprise 3types à Toulouse. C'est une plateforme qui permet de gérer facilement le contenu d'un
                                            site Web, de créer et de gérer des sites Web, et de développer la partie front-end. Smoothee offre des fonctionnalités telles que la gestion
                                            de contenu, l'édition d'objets, la création d'articles, la modification de pages, la génération de squelettes de sites web, la gestion des
                                            tickets de support, les statistiques, les outils SEO, et bien d'autres. L'objectif de Smoothee est de simplifier le processus de gestion
                                            et de création de sites Web tout en offrant des options de personnalisation et de flexibilité.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet : Réalisation de test unitaire dans le projet de 3types</h2>
                                        <p>
                                            Pendant mon stage chez 3types, j'ai travaillé sur leur projet Smoothee. J'ai conçu des
                                            tests unitaires en PHP pour évaluer le CMS de l'entreprise, en mettant l'accent sur la flexibilité, la modularité et la
                                            couverture de diverses fonctionnalités. J'ai également mis en place des tests d'accès pour différents rôles et travaillé
                                            en étroite collaboration avec mon tuteur via GitLab pour assurer une traçabilité rigoureuse. Ce stage m'a permis d'approfondir
                                            mes compétences en développement PHP et de me familiariser avec divers outils tels que Git Bash, phpMyAdmin, MySQL et
                                            GitLab. En somme, j'ai joué un rôle clé dans la garantie de la qualité de leur CMS de 3types, tout en
                                            renforçant mes compétences en développement et en gestion de projet.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>Conception de tests unitaires :</strong> capacité à concevoir des tests unitaires, en particulier en PHP, pour évaluer le bon fonctionnement d'un CMS.</li>
                                            <li><strong>Réalisation de tests complexes :</strong> capacité à effectuer une gamme variée de tests, des tests de base aux tests avancés.</li>
                                            <li><strong>Élaboration de tests d'accès :</strong> conception de tests pour différents niveaux d'accès et rôles sur un site web.</li>
                                            <li><strong>Collaboration avec des outils de gestion de version :</strong> utilisation de GitLab pour la gestion de projet et la traçabilité.</li>
                                            <li><strong>Développement en PHP :</strong> approfondissement des compétences en développement PHP.</li>
                                            <li><strong>Résolution de problèmes complexes :</strong> capacité à identifier et résoudre des problèmes techniques complexes.</li>
                                            <li><strong>Compréhension des besoins des clients :</strong> capacité à comprendre et répondre aux besoins spécifiques des clients.</li>
                                            <li><strong>Utilisation d'outils de développement :</strong> familiarité avec divers outils tels que Git Bash, phpMyAdmin, MySQL, GitHub et GitLab.</li>
                                            <li><strong>Contribution à une équipe :</strong> capacité à travailler en collaboration avec une équipe et à apporter une contribution significative à un projet.</li>
                                        </ul>

                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Répondre aux incidents et aux demandes d’assistance et d’évolution :</strong></li>
                                            <ul>
                                                <li>Traiter des demandes concernant les applications
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                            <li><strong>Mettre à disposition des utilisateurs un service informatique :</strong></li>
                                            <ul>
                                                <li>Réaliser les tests d’intégration et d’acceptation d’un service</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-1">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Graine et competence -->
            <!-- Dixiéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-stage filter-backend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/graineetcompetence/gec1.webp" alt="" class="img-fluid" id="project-image-2" data-toggle="modal" data-target="#project-modal-2">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Graine et competence</h4>
                        <p>Plugin / PHP</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-2">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators8" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators8" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators8" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="/assets/img/portfolio/graineetcompetence/gec3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="/assets/img/portfolio/graineetcompetence/gec4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators8" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators8" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Ajout plugin</li>
                                                <li><strong>Créateur </strong>: 3types</li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Code source </strong>: <a href="https://gitlab.com/Nicolas1281/3typesVersionstage/-/tree/test/Graine%20et%20comp%C3%A9tence?ref_type=heads" target="_blank">https://gitlab.com/Nicolas1281/3typesVersionstage</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Lieux de stage : <strong>3types</strong> </h2>
                                        <p>
                                            Chez 3Types, un studio de création graphique à Toulouse, l'équipe multidisciplinaire excelle dans divers domaines du
                                            design graphique. De la conception d'identités visuelles percutantes à la création de sites Web originaux,
                                            en passant par les projets éditoriaux et les besoins photographiques, le studio offre une gamme complète de services.
                                            L'équipe, composée de Timothée (développeur web), Hugo (graphiste, directeur artistique, photographe) et Mathieu
                                            (commercial, chef de projet), travaille de manière collaborative pour fournir des solutions visuelles originales.
                                            3Types incarne le rêve de trois amis passionnés qui ont concrétisé leur projet créatif.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet de l'entreprise</h2>
                                        <p>
                                            Graine et Compétence est un cabinet de recrutement qui avait déjà eu recours aux services de 3types pour le
                                            développement de leur site Web. Cependant, ils ont récemment opté pour l'intégration du CMS
                                            Smoothee, un projet conçu par la société 3Types à Toulouse. Smoothee est une plateforme complète qui facilite
                                            la gestion du contenu d'un site Web, la création et la gestion de sites, ainsi que le développement de la partie
                                            front-end.

                                            Smoothee propose diverses fonctionnalités telles que la gestion de contenu, l'édition d'objets, la création d'articles,
                                            la modification de pages, la génération de squelettes de sites web, la gestion des tickets de support,
                                            les statistiques, les outils SEO, et bien d'autres. L'objectif principal de Smoothee est de simplifier le processus
                                            de gestion et de création de sites Web tout en offrant des options de personnalisation et de flexibilité.
                                        </p>
                                    </div>
                                    <!-- Réalisations et compétences acquises -->
                                    <div class="portfolio-realisation my-3">
                                        <h2>Projet : réalisation de plugins dans le projet de 3types</h2>
                                        <p>
                                            Pendant mon stage chez 3types, j'ai travaillé sur leur projet Smoothee. J'ai conçu des plugins en PHP qui sont contrôlés par le CMS Smoothee
                                            ce qui permet à l'administrateur de créer un CMS personnalisé pour son client Graine et compétence pour qu'il puisse gérer leur site.
                                            Ce stage m'a permis d'approfondir mes compétences en développement PHP.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences acquises :</h3>
                                        <ul>
                                            <li><strong>Collaboration avec des outils de gestion de version :</strong> utilisation de GitLab pour la gestion de projet et la traçabilité.</li>
                                            <li><strong>Développement en PHP :</strong> approfondissement des compétences en développement PHP.</li>
                                            <li><strong>Compréhension des besoins des clients :</strong> capacité à comprendre et répondre aux besoins spécifiques des clients.</li>
                                            <li><strong>Utilisation d'outils de développement :</strong> familiarité avec divers outils tels que Git Bash, phpMyAdmin, MySQL, GitHub et GitLab.</li>
                                            <li><strong>Contribution à une équipe :</strong> capacité à travailler en collaboration avec une équipe et à apporter une contribution significative à un projet.</li>
                                        </ul>

                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Gérer des sauvegardes</li>
                                            </ul>
                                            <li><strong>Répondre aux incidents et aux demandes d’assistance et d’évolution :</strong></li>
                                            <ul>
                                                <li>Traiter des demandes concernant les applications
                                                </li>
                                            </ul>
                                            <li><strong>Travailler en mode projet :</strong></li>
                                            <ul>
                                                <li>Évaluer les indicateurs de suivi d’un projet et analyser les écarts</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-2">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Cusinea -->
            <!-- Onziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-ecole filter-backend filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/cuisinea/cuisinea1.webp" alt="" class="img-fluid" id="project-image-3" data-toggle="modal" data-target="#project-modal-3">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Cuisinea</h4>
                        <p>Entrainement / HTML / CSS / BOOTSTRAP / PHP</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-3" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-3">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators9" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators9" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators9" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators9" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="assets/img/portfolio/cuisinea/cuisinea2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/cuisinea/cuisinea3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/cuisinea/cuisinea4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators9" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators9" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Entrainement</li>
                                                <li><strong>Créateur </strong>: professeur de mon école </li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Lien du code source </strong>: <a href="https://gitlab.com/Nicolas1281/le-coin-des-papilles" target="_blank">https://gitlab.com/Nicolas1281/le-coin-des-papilles</a></li>
                                                <li><strong>Cours théorique </strong>: <a href="assets/img/portfolio/cuisinea/Studi - Php théorie.pdf" target="_blank">ouvrir le cours</a></li>
                                                <li><strong>Documentation du projet </strong>: <a href="assets/img/portfolio/cuisinea/Studi - Projet Php site recettes cuisine.pdf" target="_blank">ouvrir le projet</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet pratique</h2>
                                        <p>
                                            Ce projet a consisté à créer un mini-site web de recettes de cuisine utilisant HTML, CSS, PHP et Bootstrap.
                                            Le site comprend des fonctionnalités de navigation telles qu'une page d'accueil avec une sélection de recettes,
                                            une liste de catégories de recettes, ainsi que des pages individuelles pour chaque recette.
                                            Les personnes qui sont inscrites au site ont la possibilité d'ajouter, modifier et supprimer des recettes via des fonctionnalités
                                            CRUD intégrées au site.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Acquises :</h3>
                                        <ul>
                                            <li><strong>Développement en PHP et Bootstrap:</strong> Approfondissement des compétences en développement.</li>
                                        </ul>

                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences Validé :</h3>
                                        <li><strong>Organiser son développement professionnel :</strong></li>
                                        <ul>
                                            <li>Développer son projet professionnel</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-3">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Freelance Calcul -->
            <!-- Douziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-ecole filter-frontend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/calculator/calculator.webp" alt="" class="img-fluid" id="project-image-4" data-toggle="modal" data-target="#project-modal-4">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Freelance Money Calculator</h4>
                        <p>Entrainement / HTML / CSS / Javascript</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-4" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-4">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <!-- images -->
                                    <div class="col-lg-8">
                                        <img src="assets/img/portfolio/calculator/calculator.webp" class="d-block carousel-image" alt="Apps 1">
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Entrainement</li>
                                                <li><strong>Créateur </strong>: professeur de mon école </li>
                                                <li><strong>Date du projet </strong>: 2023</li>
                                                <li><strong>Lien du code source </strong>: <a href="https://gitlab.com/Nicolas1281/freelance-money" target="_blank">https://gitlab.com/Nicolas1281/freelance-money</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet pratique</h2>
                                        <p>
                                            Ce projet a consisté à créer un calculateur de revenu en étant freelance en utilisant HTML, CSS et JavaScript.
                                            Le site comprend juste une interface qui permet de calculer son revenu brut puis de connaitre son revenu net en enlevant les taxes.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences acquises :</h3>
                                        <ul>
                                            <li><strong>Développement JavaScript :</strong> approfondissement des compétences en développement.</li>
                                        </ul>

                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <li><strong>Organiser son développement professionnel :</strong></li>
                                        <ul>
                                            <li>Développer son projet professionnel</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-4">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio GLPI -->
            <!-- treiziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-ecole fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/glpi/glpi5.webp" alt="" class="img-fluid" id="project-image-5" data-toggle="modal" data-target="#project-modal-5">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Installation et utilisation de GLPI</h4>
                        <p>Entrainement / découverte / GLPI</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-5" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-5">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators10" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators10" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators10" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#carouselIndicators10" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                <button type="button" data-bs-target="#carouselIndicators10" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="assets/img/portfolio/glpi/glpi1.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/glpi/glpi2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item active">
                                                    <img src="assets/img/portfolio/glpi/glpi3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/glpi/glpi4.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators10" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators10" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Entrainement</li>
                                                <li><strong>Créateur </strong>: professeur de mon école </li>
                                                <li><strong>Date du projet </strong>: 2024</li>
                                                <li><strong>Documentation installation </strong>: <a href="assets/img/portfolio/glpi/GLPI (installation et paramétrage).pdf" target="_blank">ouvrir la documentation</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet pratique</h2>
                                        <p>
                                            Le projet a consisté à installer et à comprendre GLPI sur le système d'exploitation Ubuntu, un système open-source de gestion des
                                            services informatiques.
                                            Il comprend l'installation et la configuration de GLPI pour répondre aux besoins spécifiques de l'organisation, ainsi que
                                            la formation des utilisateurs finaux et de l'équipe de support. L'objectif est d'optimiser la gestion des ressources
                                            informatiques, des incidents et des demandes de service au sein de l'organisation.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <ol>
                                            <li><strong>Gérer le patrimoine informatique :</strong></li>
                                            <ul>
                                                <li>Mettre en place et vérifier les niveaux d'habilitation associés à un service.</li>
                                            </ul>
                                            <li><strong>Répondre aux incidents et aux demandes d’assistance et d’évolution :</strong></li>
                                            <ul>
                                                <li>Traiter des demandes concernant les services réseau et système applicatifs.</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-5">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Milmedcare Web-->
            <!-- Quatorziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-ecole filter-personnel filter-frontend filter-backend filter-web fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/milmedcareWEB/MILWEB1.webp" alt="" class="img-fluid" id="project-image-6" data-toggle="modal" data-target="#project-modal-6">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Epreuve E5 : Milmedcare WEB</h4>
                        <p>HTML / CSS / Javascript / PHP / Bootstrap</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-6" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-6">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators11" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators11" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators11" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="assets/img/portfolio/milmedcareWEB/MILWEB2.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/milmedcareWEB/MILWEB3.webp" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators11" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators11" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: examen</li>
                                                <li><strong>Créateur </strong>: Nicolas Chabaud</li>
                                                <li><strong>Date du projet </strong>: 2024</li>
                                                <li><strong>Lien du site </strong>: <a href="https://milmedcare.nicolaschabaud.com/" target="_blank">https://milmedcare.nicolaschabaud.com/</a></li>
                                                <li><strong>Lien du code source </strong>: <a href="https://gitlab.com/Nicolas1281/epreuvee5_projet1" target="_blank">https://gitlab.com/Nicolas1281/epreuvee5_projet1</a></li>
                                                <li><strong>Documentation projet 1 - Site web </strong>: <a href="/Epreuve E5/MilMedCare/Documentation projet 1 - Site web.pdf" target="_blank" class="me-3">
                                                        Documentation projet 1 - Site web.pdf
                                                    </a></li>
                                                <li><strong>Annexes 8 </strong>: <a href="/Epreuve E5/MilMedCare/8- Annexes 8 - Epreuve E5 - envt technologique - BTS SIO 2023.pdf" target="_blank" class="me-3">
                                                        8- Annexes 8 - Epreuve E5 - envt technologique - BTS SIO 2023.pdf
                                                    </a></li>
                                                <li><strong>Maquette site web </strong>: <a href="/Epreuve E5/MilMedCare/Maquette site web.png" target="_blank" class="me-3">
                                                        Maquette site web.png
                                                    </a></li>
                                                <li><strong>Diagramme de classe </strong>: <a href="/Epreuve E5/MilMedCare/Diagramme de classe.png" target="_blank" class="me-3">
                                                        Diagramme de classe.png
                                                    </a></li>
                                                <li><strong>Script </strong>: <a href="/Epreuve E5/MilMedCare/Script.png" target="_blank" class="me-3">
                                                        Script.png
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet</h2>
                                        <p>
                                            Le projet de Milmedcare vise à créer une plateforme en ligne dédiée aux membres des forces armées, offrant une solution complète
                                            pour faciliter la prise de rendez-vous médicaux dans les installations militaires. La plateforme permettra aux utilisateurs de
                                            créer un compte sécurisé, de programmer des rendez-vous médicaux en fonction de la spécialité recherchée et de la localisation
                                            dans les bases aériennes, les casernes, les bases navales et les hôpitaux militaires. De plus, les militaires auront accès à leur
                                            dossier médical, ce qui leur permettra de mieux gérer leur santé et leur suivi médical. Cette initiative vise à simplifier et à
                                            optimiser l'accès aux soins de santé pour les membres des forces armées, contribuant ainsi à leur bien-être et à leur efficacité
                                            opérationnelle.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Projet : technique</h2>
                                        <p>
                                            Pour le projet Milmedcare, j'ai pris en charge plusieurs aspects techniques essentiels. J'ai d'abord réalisé des maquettes
                                            détaillées pour les interfaces utilisateurs, puis les ai intégrées dans le code pour assurer une expérience utilisateur fluide.
                                            En parallèle, j'ai élaboré un diagramme de classe pour organiser la structure des données de l'application. J'ai ensuite mis en
                                            place la base de données en rédigeant un script SQL et en configurant un service de base de données pour stocker les informations
                                            de manière sécurisée. Enfin, j'ai assuré le déploiement en ligne de l'application en choisissant un service d'hébergement
                                            approprié et en mettant en place un système d'authentification sécurisé pour contrôler l'accès des utilisateurs. Ces étapes
                                            ont été cruciales pour garantir le bon fonctionnement et la sécurité de l'application Milmedcare, facilitant ainsi la gestion
                                            des rendez-vous médicaux et des dossiers médicaux pour les membres des forces armées.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <ol>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à l'évolution d'un site Web exploitant les données de l'organisation.</li>
                                            </ul>
                                            <li><strong>Mettre à disposition des utilisateurs un service informatique :</strong></li>
                                            <ul>
                                                <li>Déployer un service</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-6">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Portfolio Milmedcare mobile-->
            <!-- Quinziéme projet du portfolio -->
            <div class="col-lg-3 portfolio-item filter-ecole filter-personnel filter-frontend filter-backend filter-mobile fade-up-on-scroll">
                <div class="Projetcenter interactive-effect">
                    <!-- Vidéo du projet -->
                    <img src="assets/img/portfolio/MilmedcareMobile/MILMOBILE1.webp" style="width:26%; " alt="" class="img-fluid" id="project-image-7" data-toggle="modal" data-target="#project-modal-7">
                    <!-- Informations du projet -->
                    <div class="portfolio-info mt-2">
                        <h4>Epreuve E5 : Milmedcare Mobile</h4>
                        <p>Dart / Flutter</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-7" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails du projet</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-7">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <!-- Carrousel d'images -->
                                        <div id="carouselIndicators12" class="carousel slide">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#carouselIndicators12" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#carouselIndicators12" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            </div>
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="assets/img/portfolio/MilmedcareMobile/MILMOBILE2.webp" style="width: 25%;" class="d-block carousel-image" alt="...">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="assets/img/portfolio/MilmedcareMobile/MILMOBILE3.webp" style="width: 25%;" class="d-block carousel-image" alt="...">
                                                </div>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators12" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators12" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- Informations sur le projet -->
                                        <div class="portfolio-info">
                                            <h3>Information du projet</h3>
                                            <ul>
                                                <li><strong>Catégorie </strong>: Examen BTS</li>
                                                <li><strong>Créateur </strong>: Nicolas Chabaud </li>
                                                <li><strong>Date du projet </strong>: 2024</li>
                                                <li><strong>Lien du code source </strong>: <a href="https://gitlab.com/Nicolas1281/epreuvee5_projet2" target="_blank">https://gitlab.com/Nicolas1281/epreuvee5_projet2</a></li>
                                                <li><strong>Documentation projet 2 - Appli Mobile </strong>: <a href="/Epreuve E5/MilMedCare/Documentation projet 2 - Appli Mobile.pdf" target="_blank" class="me-3">
                                                        Documentation projet 2 - Appli Mobile.pdf
                                                    </a></li>
                                                <li><strong>Annexes 8 </strong>: <a href="/Epreuve E5/MilMedCare/8- Annexes 8 - Epreuve E5 - envt technologique - BTS SIO 2023.pdf" target="_blank" class="me-3">
                                                        8- Annexes 8 - Epreuve E5 - envt technologique - BTS SIO 2023.pdf
                                                    </a></li>
                                                <li><strong>Maquette application mobile </strong>: <a href="/Epreuve E5/MilMedCare/Maquette application mobile.png" target="_blank" class="me-3">
                                                        Maquette application mobile.png
                                                    </a></li>
                                                <li><strong>Diagramme de classe </strong>: <a href="/Epreuve E5/MilMedCare/Diagramme de classe.png" target="_blank" class="me-3">
                                                        Diagramme de classe.png
                                                    </a></li>
                                                <li><strong>Script </strong>: <a href="/Epreuve E5/MilMedCare/Script.png" target="_blank" class="me-3">
                                                        Script.png
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Description du projet</h2>
                                        <p>
                                            Le projet de Milmedcare vise à créer une application mobile dédiée aux membres des forces armées.
                                            De plus, les militaires auront accès à leur dossier médical, ce qui leur permettra de mieux gérer leur santé et leur suivi médical.
                                            Cette initiative vise à simplifier et à optimiser l'accès aux soins de santé pour les membres des forces armées,
                                            contribuant ainsi à leur bien-être et à leur efficacité opérationnelle.
                                        </p>
                                    </div>
                                    <div class="portfolio-description my-3">
                                        <!-- Description du projet -->
                                        <h2>Projet : technique</h2>
                                        <p>
                                            Pour répondre aux besoins de Milmedcare, j'ai opté pour l'utilisation du framework Dart Flutter pour le développement de
                                            l'application mobile. En exploitant les fonctionnalités de Flutter, j'ai pu créer des interfaces utilisateur intuitives et
                                            réactives, compatibles avec les systèmes d'exploitation iOS et Android. J'ai intégré des fonctionnalités cruciales telles que
                                            la visualisation de son dossier médical et de son profil via des requêtes API sécurisées avec le
                                            protocole HTTPS. Cette approche technique garantit la rapidité de développement, la cohérence visuelle et la performance de
                                            l'application, offrant ainsi une expérience utilisateur optimale aux membres des forces armées.
                                        </p>
                                    </div>
                                    <div class="portfolio-realisation my-3">
                                        <h3>Compétences validées :</h3>
                                        <ol>
                                            <li><strong>Développer la présence en ligne de l’organisation :</strong></li>
                                            <ul>
                                                <li>Participer à l'évolution d'un site Web exploitant les données de l'organisation.</li>
                                            </ul>
                                            <li><strong>Mettre à disposition des utilisateurs un service informatique :</strong></li>
                                            <ul>
                                                <li>Déployer un service</li>
                                            </ul>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-7">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="fade-up-on-scroll">
            <img width="30" height="30" src="https://img.icons8.com/fluency/48/general-warning-sign.png" alt="general-warning-sign" />
            <b>Veuillez cliquer sur l'image de chaque projet pour afficher les détails. Ne cliquez pas sur les textes des projets, car cela n'ouvrira pas les détails.</b>
            <img width="30" height="30" src="https://img.icons8.com/fluency/48/general-warning-sign.png" alt="general-warning-sign" />
        </div>

    </div>
</section>