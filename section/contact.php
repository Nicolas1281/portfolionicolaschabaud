<!-- Section Contact -->
<section id="contact" class="g-5 py-5 pad form-position fade-up-on-scroll">
    <!-- Conteneur principal -->
    <div class="container">
        <!-- Titre de la section -->
        <div class="section-title">
            <h2>Contactez-moi</h2>
        </div>
        <!-- Texte d'introduction -->
        <p class="text-center w-responsive mx-auto mb-5 text-dark">Avez-vous des questions ? N'hésitez pas à me contacter directement.</p>
        <!-- Conteneur de la grille -->
        <div class="row">
            <!-- Formulaire de contact -->
            <div class="mb-md-0 mb-5">
                <form id="contact-form" class="mb-3" name="contact-form" action="forms/contact.php" method="POST" enctype="multipart/form-data">
                    <!-- Ligne pour les champs Nom et Email -->
                    <div class="row">
                        <!-- Champ Nom -->
                        <div class="col-md-6 mb-3">
                            <div class="md-form mb-0">
                                <label for="name" class="text-dark">Votre nom</label>
                                <input type="text" id="name" name="name" class="form-control">
                            </div>
                        </div>
                        <!-- Champ Email -->
                        <div class="col-md-6 mb-3">
                            <div class="md-form mb-0">
                                <label for="email" class="text-dark">Votre adresse e-mail</label>
                                <input type="text" id="email" name="email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- Ligne pour le champ Sujet -->
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="md-form mb-0">
                                <label for="subject" class="text-dark">Sujet</label>
                                <input type="text" id="subject" name="subject" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- Ligne pour le champ Message -->
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="md-form">
                                <label for="message" class="text-dark">Votre message</label>
                                <textarea id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="error-message" class="alert alert-danger text-center" style="display:none;"></div>
                    <div id="success-message" class="alert alert-success text-center" style="display:none;"></div>
                    <!-- Bouton d'envoi -->
                    <div class="text-center text-md-left my-4">
                        <button type="submit" class="btn btn-primary interactive-effect" id="submitBtn" aria-label="Envoyer le formulaire de contact" title="Cliquez pour envoyer le formulaire de contact">Envoyer</button>
                    </div>
                </form>
            </div>
            <!-- Carte Google Maps -->
            <div class="text-center">
                <div class="map-container">
                    <iframe title="Carte Google Maps de Salon de provence" class="responsive-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46195.94203592072!2d5.007838800189335!3d43.64304211479871!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b60033a801faa1%3A0x40819a5fd96ff20!2sSalon-de-Provence!5e0!3m2!1sfr!2sfr!4v1700155429692!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>