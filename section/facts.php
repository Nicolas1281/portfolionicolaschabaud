<!-- Section des Chiffres Clés -->
<section id="facts" class="facts py-5">
    <!-- Conteneur principal -->
    <div class="container fade-up-on-scroll">
        <!-- Titre et introduction de la section -->
        <div class="section-title">
            <h2>Chiffres Clés</h2>
            <!-- Description et introduction aux statistiques -->
            <p>
                Découvrez quelques statistiques et faits marquants qui illustrent mon parcours et mes compétences en développement. Ces chiffres sont le reflet
                de mon engagement et de ma passion pour le monde de la technologie.
            </p>
        </div>
        <!-- Ligne pour organiser les boîtes de comptage -->
        <div class="row justify-content-center">
            <!-- Première boîte : Nombre de projets -->
            <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                <div class="count-box d-flex flex-column justify-content-center align-items-center">
                    <!-- Icône SVG -->
                    <i><svg width="40" height="40" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.5 18.234a.98.98 0 0 1-.648-.244l-6-5.25a.985.985 0 0 1 0-1.48l6-5.25a.984.984 0 1 1 1.296 1.48L2.994 12l5.154 4.509a.984.984 0 0 1-.649 1.725Z"></path>
                            <path d="M16.5 18.234a.984.984 0 0 1-.648-1.725l5.154-4.51-5.153-4.508a.984.984 0 1 1 1.296-1.482l6 5.25a.985.985 0 0 1 0 1.482l-6 5.25a.979.979 0 0 1-.648.243Z"></path>
                            <path d="M9.75 20.481a.985.985 0 0 1-.943-1.266l4.5-15a.984.984 0 1 1 1.885.563l-4.5 15a.984.984 0 0 1-.942.703Z"></path>
                        </svg></i>
                    <!-- Compteur et texte -->
                    <span class="purecounter mt-3" data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="3000">15</span>
                    <p>Projects</p>
                </div>
            </div>
            <!-- Deuxième boîte : Heures de formation -->
            <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                <div class="count-box d-flex flex-column justify-content-center align-items-center">
                    <!-- Icône SVG -->
                    <i><svg width="40" height="40" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.825 21.75h10.35c.927 0 1.666-.764 1.566-1.643-.645-5.67-4.491-5.576-4.491-8.107 0-2.531 3.895-2.39 4.49-8.107.094-.88-.638-1.643-1.566-1.643H6.825c-.928 0-1.658.763-1.566 1.643C5.854 9.61 9.75 9.422 9.75 12c0 2.578-3.846 2.438-4.49 8.107-.1.88.638 1.643 1.566 1.643Z"></path>
                            <path fill="currentColor" stroke="none" d="M16.091 20.25H7.927c-.731 0-.937-.844-.424-1.367 1.24-1.258 3.746-2.159 3.746-3.602V10.5c0-.93-1.781-1.64-2.883-3.15-.182-.249-.164-.6.299-.6h6.69c.394 0 .48.348.3.598-1.086 1.511-2.906 2.217-2.906 3.152v4.781c0 1.431 2.612 2.203 3.769 3.603.466.565.303 1.366-.427 1.366Z"></path>
                        </svg></i>
                    <!-- Compteur et texte -->
                    <span class="purecounter mt-3" data-purecounter-start="0" data-purecounter-end="360" data-purecounter-duration="3000">360</span>
                    <p>Heures de formation</p>
                </div>
            </div>
            <!-- Troisième boîte : Semaines de stage -->
            <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                <div class="count-box d-flex flex-column justify-content-center align-items-center">
                    <!-- Icône SVG -->
                    <i><svg width="40" height="40" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.5 3.75h-15A2.25 2.25 0 0 0 2.25 6v13.5a2.25 2.25 0 0 0 2.25 2.25h15a2.25 2.25 0 0 0 2.25-2.25V6a2.25 2.25 0 0 0-2.25-2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M13.875 12a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M17.625 12a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M13.875 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M17.625 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M6.375 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M10.125 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M6.375 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M10.125 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path fill="currentColor" stroke="none" d="M13.875 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                            <path d="M6 2.25v1.5"></path>
                            <path d="M18 2.25v1.5"></path>
                            <path d="M21.75 7.5H2.25"></path>
                        </svg></i>
                    <!-- Compteur et texte -->
                    <span class="purecounter mt-3" data-purecounter-start="0" data-purecounter-end="10" data-purecounter-duration="3000">10</span>
                    <p>Semaines de stage</p>
                </div>
            </div>
        </div>
    </div>
</section>