<!-- Section veille -->
<section id="veille" class="veille section-bg py-5">
    <!-- Conteneur principal -->
    <div class="container">
        <!-- Titre de la section -->
        <div class="section-title fade-up-on-scroll">
            <h2>Veille technologique</h2>
        </div>
        <div class="my-5 fade-up-on-scroll">
            <h3>Mon sujet : <strong>la veille technologique en intelligence artificielle (IA)</strong></h3>
            <p>
                Dans le cadre de mon BTS SIO, l'épreuve E4 nécessite une veille technologique approfondie. Pour ce portfolio, je me concentre sur le domaine de
                l'intelligence artificielle (IA), qui est un secteur en constante évolution et qui a un impact significatif sur diverses industries.
            </p>
            <h4>Qu'est-ce que la veille technologique en IA ?</h4>
            <p>
                La veille technologique en IA consiste à suivre les dernières tendances, innovations et avancées dans le domaine de l'intelligence artificielle.
                Cela permet de rester à jour et compétitif dans un environnement en constante évolution.
            </p>
            <h3>Mes outils</h3>
            <p>
                Pour rester à jour sur les dernières tendances en développement, j'utilise une combinaison d'outils de veille technologique. Daily.dev me permet de découvrir rapidement
                les dernières actualités et articles pertinents, directement dans mon navigateur. Cet outil me donne une vue complète et actualisée du monde du développement. J'utilise
                également d'autre outils comme stackoverflow , Reddit et InfoQ .
            </p>
        </div>

        <div class="row my-5">
            <!-- Article 1-->
            <div class="col-lg-3 portfolio-item fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- image -->
                    <img src="assets/img/article/overflowai.webp" alt="" class="img-fluid" id="project-image-8" data-toggle="modal" data-target="#project-modal-8">
                    <!-- Informations de l'article -->
                    <div class="portfolio-info mt-2">
                        <h4 class="text-center">
                            Announcing OverflowAI
                        </h4>
                        <p class="text-center mt-3">Auteur : Prashanth Chandrasekar</p>
                        <p class="text-center">27 juillet 2023</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-8" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails de l'article</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-8">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">

                                    <div class="portfolio-description my-3">
                                        <!-- Description de l'article -->
                                        <h2>Description de l'article</h2>
                                        <p>
                                            Stack Overflow annonce le lancement d'OverflowAI, intégrant l'IA générative dans Stack Overflow for Teams et
                                            introduisant de nouveaux produits. Les améliorations comprennent une recherche sémantique sur le site public et
                                            une extension IDE pour Visual Studio Code. OverflowAI propose également une ingestion de connaissances d'entreprise
                                            et une intégration avec Slack. La communauté bénéficiera de GenAI Stack Exchange et de discussions dans le cadre du
                                            Natural Language Processing (NLP) Collective. Stack Overflow se concentre sur la confiance et l'attribution, invitant
                                            les développeurs à participer aux tests et à fournir des retours pour guider l'évolution de la plateforme.
                                        </p>
                                    </div>
                                    <!-- lien officiel -->
                                    <div class="portfolio-description my-3">
                                        <h2>Liens</h2>
                                        <ul>
                                            <li>
                                                <strong>Lien vers l'article :</strong> <a href="https://stackoverflow.blog/2023/07/27/announcing-overflowai/?ref=dailydev" target="_blank">https://stackoverflow.blog/2023/07/27/announcing-overflowai/?ref=dailydev</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-8">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Article 2-->
            <div class="col-lg-3 portfolio-item fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- image -->
                    <img src="assets/img//article/GitHub-Copilot_blog-header.webp" alt="" class="img-fluid" id="project-image-9" data-toggle="modal" data-target="#project-modal-9">
                    <!-- Informations de l'article -->
                    <div class="portfolio-info mt-2">
                        <h4 class="text-center">
                            GitHub Copilot
                        </h4>
                        <p class="text-center mt-3">Auteur : Ryan Daws</p>
                        <p class="text-center">21 septembre 2023</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-9" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails de l'article</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-9">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">
                                    <div class="portfolio-description my-3">
                                        <!-- Description de l'article -->
                                        <h2>Description de l'article</h2>
                                        <p>
                                            GitHub a ouvert Copilot Chat à tous les développeurs, marquant une ère de développement logiciel alimenté par l'IA. Initialement lancé pour
                                            les utilisateurs 'Business' en juillet, Copilot Chat assiste les développeurs dans leur langage naturel, réduisant les tâches répétitives.
                                            Les développeurs peuvent l'utiliser dans leur environnement de développement intégré (IDE) pour explorer de nouveaux langages, résoudre des
                                            bugs et rechercher des réponses de codage, améliorant ainsi la concentration et la productivité. L'outil offre des conseils en temps réel,
                                            une analyse de code et des suggestions de sécurité, visant à simplifier les processus de débogage. GitHub envisage que Copilot Chat
                                            démocratise le développement logiciel en permettant aux développeurs du monde entier d'apprendre et de construire dans leur langage
                                            préféré. La plateforme voit le langage naturel comme un outil de programmation universel, et Copilot Chat est une étape significative
                                            vers cette vision.
                                        </p>
                                    </div>
                                    <!-- lien officiel -->
                                    <div class="portfolio-description my-3">
                                        <h2>Liens</h2>
                                        <ul>
                                            <li>
                                                <strong>Site officiel de GitHub Copilot:</strong> <a href="https://copilot.github.com/" target="_blank">https://copilot.github.com/</a>
                                            </li>
                                            <li>
                                                <strong>Lien vers l'article :</strong> <a href="https://www.developer-tech.com/news/2023/sep/21/github-opens-copilot-chat-all-developers/" target="_blank">https://www.developer-tech.com/news/2023/sep/21/github-opens-copilot-chat-all-developers/</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-9">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Article 3-->
            <div class="col-lg-3 portfolio-item fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- image -->
                    <img src="assets/img/article/ai.webp" alt="" class="img-fluid" id="project-image-10" data-toggle="modal" data-target="#project-modal-10">
                    <!-- Informations de l'article -->
                    <div class="portfolio-info mt-2">
                        <h4 class="text-center">
                            ChatGPT
                        </h4>
                        <p class="text-center mt-3">Auteur : Open AI</p>
                        <p class="text-center">25 septembre 2023</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-10" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails de l'article</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-10">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">

                                    <div class="portfolio-description my-3">
                                        <!-- Description de l'article -->
                                        <h2>Description de l'article</h2>
                                        <p>
                                            OpenAI déploie de nouvelles capacités de voix et d'image dans ChatGPT, offrant une interface plus intuitive.
                                            Les utilisateurs Plus et Entreprise bénéficieront de ces fonctionnalités au cours des deux prochaines semaines,
                                            suivis d'un déploiement plus large. La voix permet des conversations interactives, tandis que l'image permet de
                                            montrer des images pour résoudre des problèmes ou analyser des données. OpenAI adopte une approche progressive pour
                                            garantir la sécurité et la responsabilité, en limitant la capacité d'analyse des personnes par ChatGPT et en
                                            soulignant les limitations du modèle.
                                        </p>
                                    </div>
                                    <!-- lien officiel -->
                                    <div class="portfolio-description my-3">
                                        <h2>Liens officiels</h2>
                                        <ul>
                                            <li>
                                                <strong>Lien vers l'article :</strong> <a href="https://openai.com/blog/chatgpt-can-now-see-hear-and-speak#OpenAI" target="_blank">https://openai.com/blog/chatgpt-can-now-see-hear-and-speak#OpenAI</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-10">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Article 4-->
            <div class="col-lg-3 portfolio-item fade-up-on-scroll">
                <div class="interactive-effect">
                    <!-- image -->
                    <img src="assets/img/article/ai3.webp" alt="" class="img-fluid" id="project-image-11" data-toggle="modal" data-target="#project-modal-11">
                    <!-- Informations de l'article -->
                    <div class="portfolio-info mt-2">
                        <h4 class="text-center">
                            11 AI-Powered Image
                        </h4>
                        <p class="text-center mt-3">Auteur : Editorial Team</p>
                        <p class="text-center">23 février 2024</p>
                    </div>
                </div>
            </div>

            <!-- Modal pour les détails du projet -->
            <div class="modal fade" id="project-modal-11" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <!-- En-tête du modal -->
                        <div class="modal-header">
                            <h5 class="modal-title">Détails de l'article</h5>
                            <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-11">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- Contenu du modal -->
                        <section class="portfolio-details my-3">
                            <div class="container">
                                <div class="row">

                                    <div class="portfolio-description my-3">
                                        <!-- Description de l'article -->
                                        <h2>Description de l'article</h2>
                                        <p>
                                            Les générateurs d'images alimentés par l'intelligence artificielle sont des outils puissants qui peuvent créer des visuels uniques à partir
                                            de simples instructions textuelles. Ils offrent une solution économique et gain de temps pour divers secteurs tels que le design, le
                                            marketing, l'éducation, etc. Ces outils permettent aux utilisateurs de gagner du temps en évitant la création manuelle d'images. Ils
                                            offrent également une variété d'avantages pour différentes industries, stimulant la créativité des artistes et des designers, aidant
                                            les éducateurs à créer du matériel pédagogique attrayant, et permettant aux marketeurs d'améliorer leur contenu avec des images percutantes.
                                            En termes de tarification, les générateurs d'images AI offrent une gamme d'options, de plans gratuits avec des fonctionnalités limitées à
                                            des abonnements mensuels ou annuels donnant accès à des fonctionnalités avancées et des images haute résolution. En résumé, les générateurs
                                            d'images AI en 2024 révolutionnent la création de contenu visuel, offrant polyvalence, gain de temps et possibilités infinies pour les
                                            utilisateurs de différents secteurs.
                                        </p>
                                    </div>
                                    <!-- lien officiel -->
                                    <div class="portfolio-description my-3">
                                        <h2>Liens officiels</h2>
                                        <ul>
                                            <li>
                                                <strong>Lien vers l'article :</strong> <a href="https://towardsai.net/p/machine-learning/top-11-ai-powered-image-generators-in-2024?ref=dailydev" target="_blank">https://towardsai.net/p/machine-learning/top-11-ai-powered-image-generators-in-2024?ref=dailydev</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Pied de page du modal -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-11">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>