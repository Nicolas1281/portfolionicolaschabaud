<!-- Section du CV -->
<section id="resume" class="resume py-5 fade-up-on-scroll">
    <!-- Conteneur principal -->
    <div class="container">
        <!-- Titre et introduction de la section -->
        <div class="section-title">
            <h2>CV</h2>
            <!-- Description et objectifs professionnels -->
            <p>
                Après l'obtention de mon BTS, je serais pleinement engagé dans la recherche d'un poste de développeur dès le début de novembre 2024. En parallèle, j'ai l'intention de
                poursuivre mes études en vue d'obtenir une licence trois. Je recherche une opportunité professionnelle qui me permettra de concilier travail et études, en accord avec
                mes objectifs académiques et professionnels.
            </p>
        </div>
        <div class="cv-container">
            <a href="/CV développeur.pdf" target="_blank" class="cv-button interactive-effect">
                Voir mon CV
            </a>
        </div>
        <!-- Contenu du CV organisé en deux colonnes -->
        <div class="row d-flex align-items-stretch">
            <!-- Colonne de gauche -->
            <div class="col-lg-6 d-flex flex-column">
                <!-- Bloc du résumé personnel -->
                <div class="resume-border my-3">
                    <h3 class="resume-title">Résumé</h3>
                    <div class="resume-item">
                        <!-- Informations personnelles -->
                        <h4>Nicolas CHABAUD</h4>
                        <p><em>Développeur passionné en reconversion</em></p>
                        <ul>
                            <li>Salon de Provence</li>
                            <li>0640658435</li>
                            <li>nicolas.chab@hotmail.fr</li>
                        </ul>
                    </div>
                </div>
                <!-- Bloc de l'éducation et des formations -->
                <div class="resume-border my-3 flex-grow-1">
                    <h3 class="resume-title">Éducation</h3>
                    <!-- Élément de formation : BTS SIO -->
                    <div class="resume-item mt-3">
                        <h4>
                            Brevet Technicien Supérieur (BTS) — Services Informatiques aux Organisations (SIO), Option Solutions Logicielles et Applications Métiers (SLAM)
                        </h4>
                        <p>2022 – 2024</p>
                        <p><em>STUDI</em></p>
                        <p>
                            Actuellement en formation, je suis en train d'acquérir des compétences avancées en développement informatique. Cette expérience m'offre la possibilité de me
                            spécialiser dans des domaines clés de l'informatique, tout en me préparant à devenir un professionnel polyvalent dans ce secteur en pleine évolution.
                        </p>
                    </div>
                    <!-- Élément de formation : Baccalauréat Pro Aéronautique -->
                    <div class="resume-item">
                        <h4>Baccalauréat professionnel en aéronautique, option avionique</h4>
                        <p>2017 – 2019</p>
                        <p><em>École d'enseignement technique de l'Armée de l'Air et de l'Espace, Saintes</em></p>
                        <p>
                            Pendant cette formation, j'ai développé des compétences spécialisées en mécanique avionique. De plus, mon expérience au sein de l'Armée de l'Air et de l'Espace
                            m'a permis d'acquérir des compétences militaires cruciales, telles que la discipline, le travail d'équipe et la gestion du stress.
                        </p>
                    </div>
                </div>
            </div>
            <!-- Colonne de droite -->
            <div class="col-lg-6 d-flex flex-column">
                <!-- Bloc de l'expérience professionnelle -->
                <div class="resume-border my-3 flex-grow-1">
                    <h3 class="resume-title">Expérience Professionnelle</h3>
                    <!-- Élément d'expérience : Stage de Développement -->
                    <div class="resume-item mt-3">
                        <h4>Stage de développement</h4>
                        <p>Période : 13/02/2023 au 24/03/2023 &amp; 16/10/2023 au 10/11/2023</p>
                        <p><em>3types</em></p>
                        <ul>
                            <li>
                                <h5>Tests unitaires en PHP</h5>
                                <ul>
                                    <li>
                                        Tests pour le CMS 3types : inscription, connexion, suppression de comptes.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <h5>Collaboration et traçabilité</h5>
                                <ul>
                                    <li>
                                        Collaboration avec l'équipe de dev ; suivi rigoureux des « issues ».
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <h5>Développement Web</h5>
                                <ul>
                                    <li>
                                        Création de pages en HTML et CSS.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <h5>Mentions légales</h5>
                                <ul>
                                    <li>
                                        Pages pour le site bilingue de Hugo Zely.
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <h5>Amélioration des compétences</h5>
                                <ul>
                                    <li>Codage, gestion du temps, communication à distance.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Élément d'expérience : Mécanicien Avionique -->
                    <div class="resume-item">
                        <h4>Mécanicien avionique</h4>
                        <p>Période :2020 – 2024</p>
                        <p><em>Employeur : Armée de l'air et de l'espace - Istres</em></p>
                        <ul>
                            <li>
                                <h5>Maintenance d'avions</h5>
                                <ul>
                                    <li>Contrôles et réparations. Maintenance système.</li>
                                </ul>
                            </li>
                            <li>
                                <h5>Missions courtes et longues</h5>
                                <ul>
                                    <li>Support technique et ravitaillement en vol.</li>
                                </ul>
                            </li>
                            <li>
                                <h5>Compétences militaires</h5>
                                <ul>
                                    <li>Formation et préparation.</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>