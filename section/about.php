<!-- Section "À propos" -->
<section id="about" class="py-5">
    <!-- Conteneur principal -->
    <div class="container fade-up-on-scroll">
        <!-- Titre de la section -->
        <div class="section-title my-5">
            <h2>À propos</h2>
            <!-- Description générale -->
            <p>
                Je suis Nicolas CHABAUD, passionné d'informatique et en reconversion professionnelle. Ancien mécanicien avionique dans l'Armée de l'Air, je me suis inscrit
                en BTS Services Informatiques aux Organisations (SIO), spécialisé en Solutions Logicielles et Applications Métiers (SLAM). Mon objectif : devenir un
                développeur polyvalent pour créer des solutions web, mobiles et logicielles sécurisées et fiables.
            </p>
        </div>
        <!-- Contenu de la section -->
        <div class="row">
            <!-- Colonne de gauche : Photo de profil -->
            <div class="col-lg-4 about-center">
                <img src="assets/img/Nicolas.webp" class="image-response" alt="Photo de profil">
            </div>
            <!-- Colonne de droite : Informations supplémentaires -->
            <div class="col-lg-8 pt-4 pt-lg-0 justify-content-center">
                <h2>Développeur informatique</h2>
                <h4 class="my-4">Informations personnelles :</h4>
                <div class="row m">
                    <!-- Première moitié des informations -->
                    <div class="col-lg-6 icone-center">
                        <!-- Date de naissance -->
                        <p>
                            <i><svg class="icone" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 448 512">
                                    <path d="M448 384c-28.02 0-31.26-32-74.5-32-43.43 0-46.825 32-74.75 32-27.695 0-31.454-32-74.75-32-42.842 0-47.218 32-74.5 32-28.148 0-31.202-32-74.75-32-43.547 0-46.653 32-74.75 32v-80c0-26.5 21.5-48 48-48h16V112h64v144h64V112h64v144h64V112h64v144h16c26.5 0 48 21.5 48 48v80zm0 128H0v-96c43.356 0 46.767-32 74.75-32 27.951 0 31.253 32 74.75 32 42.843 0 47.217-32 74.5-32 28.148 0 31.201 32 74.75 32 43.357 0 46.767-32 74.75-32 27.488 0 31.252 32 74.5 32v96zM96 96c-17.75 0-32-14.25-32-32 0-31 32-23 32-64 12 0 32 29.5 32 56s-14.25 40-32 40zm128 0c-17.75 0-32-14.25-32-32 0-31 32-23 32-64 12 0 32 29.5 32 56s-14.25 40-32 40zm128 0c-17.75 0-32-14.25-32-32 0-31 32-23 32-64 12 0 32 29.5 32 56s-14.25 40-32 40z" />
                                </svg></i> <strong>Date de Naissance :</strong> <span>12 janvier 2001</span>
                        </p>
                        <!-- Site Web -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 2.25A9.75 9.75 0 0 0 2.25 12c0 5.384 4.365 9.75 9.75 9.75 5.384 0 9.75-4.366 9.75-9.75 0-5.385-4.366-9.75-9.75-9.75Z"></path>
                                    <path d="M12 2.25c-2.722 0-5.28 4.365-5.28 9.75 0 5.384 2.56 9.75 5.281 9.75 2.722 0 5.282-4.366 5.282-9.75 0-5.385-2.56-9.75-5.282-9.75Z"></path>
                                    <path d="M5.5 5.5C7.293 6.773 9.55 7.532 12 7.532c2.451 0 4.708-.76 6.5-2.032"></path>
                                    <path d="M18.5 18.5c-1.792-1.272-4.049-2.031-6.5-2.031-2.45 0-4.707.759-6.5 2.031"></path>
                                    <path d="M12 2.25v19.5"></path>
                                    <path d="M21.75 12H2.25"></path>
                                </svg></i> <strong>Site Web :</strong> <a href="https://nicolaschabaud.com/" target="_blank">https://nicolaschabaud.com/</a>
                        </p>
                        <!-- téléphone -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.75.75h-7.5A2.25 2.25 0 0 0 6 3v18a2.25 2.25 0 0 0 2.25 2.25h7.5A2.25 2.25 0 0 0 18 21V3A2.25 2.25 0 0 0 15.75.75Z"></path>
                                    <path d="M8.25.75h1.125a.375.375 0 0 1 .375.375.75.75 0 0 0 .75.75h3a.75.75 0 0 0 .75-.75.375.375 0 0 1 .375-.375h1.125"></path>
                                </svg></i> <strong>Téléphone :</strong> <a href="tel:0640658435">0640658435</a>
                        </p>
                        <!-- Ville -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 2.25c-3.727 0-6.75 2.878-6.75 6.422 0 4.078 4.5 10.54 6.152 12.773a.739.739 0 0 0 1.196 0c1.652-2.231 6.152-8.692 6.152-12.773 0-3.544-3.023-6.422-6.75-6.422Z"></path>
                                    <path d="M12 11.25a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5Z"></path>
                                </svg></i> <strong>Ville :</strong> <span>Salon-de-Provence, France</span>
                        </p>
                    </div>
                    <!-- Deuxième moitié des informations -->
                    <div class="col-lg-6 icone-center">
                        <!-- Âge -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6.825 21.75h10.35c.927 0 1.666-.764 1.566-1.643-.645-5.67-4.491-5.576-4.491-8.107 0-2.531 3.895-2.39 4.49-8.107.094-.88-.638-1.643-1.566-1.643H6.825c-.928 0-1.658.763-1.566 1.643C5.854 9.61 9.75 9.422 9.75 12c0 2.578-3.846 2.438-4.49 8.107-.1.88.638 1.643 1.566 1.643Z"></path>
                                    <path fill="currentColor" stroke="none" d="M16.091 20.25H7.927c-.731 0-.937-.844-.424-1.367 1.24-1.258 3.746-2.159 3.746-3.602V10.5c0-.93-1.781-1.64-2.883-3.15-.182-.249-.164-.6.299-.6h6.69c.394 0 .48.348.3.598-1.086 1.511-2.906 2.217-2.906 3.152v4.781c0 1.431 2.612 2.203 3.769 3.603.466.565.303 1.366-.427 1.366Z"></path>
                                </svg></i> <strong>Âge :</strong> <span>22</span>
                        </p>
                        <!-- Diplôme -->
                        <p>
                            <i><img class="icone" src="/assets/img/icone/certificat.png" width="30" height="30" alt=""></i> <strong>Diplôme :</strong> <span>Bac Pro Aéronautique option Avionique</span>
                        </p>
                        <!-- Email -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m20.7 8.044-8.19-4.042a1.151 1.151 0 0 0-1.02 0L3.3 8.044a1.875 1.875 0 0 0-1.05 1.677v8.654c0 1.035.85 1.875 1.9 1.875h15.702c1.05 0 1.9-.84 1.9-1.875V9.72A1.875 1.875 0 0 0 20.7 8.044v0Z"></path>
                                    <path d="m18.625 17.247-6.06-4.713a1.125 1.125 0 0 0-1.38 0l-6.06 4.713"></path>
                                    <path d="M14.5 13.828 20.875 9"></path>
                                    <path d="m2.875 9 6.516 4.922"></path>
                                </svg></i> <strong>Email :</strong> <a href="mailto:nicolas.chab@hotmail.fr">nicolas.chab@hotmail.fr</a>
                        </p>
                        <!-- Disponibilité -->
                        <p>
                            <i><svg class="icone" width="30" height="30" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M19.5 3.75h-15A2.25 2.25 0 0 0 2.25 6v13.5a2.25 2.25 0 0 0 2.25 2.25h15a2.25 2.25 0 0 0 2.25-2.25V6a2.25 2.25 0 0 0-2.25-2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M13.875 12a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M17.625 12a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M13.875 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M17.625 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M6.375 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M10.125 15.75a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M6.375 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M10.125 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path fill="currentColor" stroke="none" d="M13.875 19.5a1.125 1.125 0 1 0 0-2.25 1.125 1.125 0 0 0 0 2.25Z"></path>
                                    <path d="M6 2.25v1.5"></path>
                                    <path d="M18 2.25v1.5"></path>
                                    <path d="M21.75 7.5H2.25"></path>
                                </svg></i> <strong>Disponibilité :</strong> <span>début novembre 2024</span>
                        </p>
                    </div>
                </div>
                <!-- Vision personnelle -->
                <h4 class="my-4">Ma vision</h4>
                <p>
                    Je suis déterminé à rester à la pointe des dernières tendances en développement pour continuer à enrichir mes compétences. Mon expérience précédente dans
                    l'Armée de l'Air m'a fourni des compétences essentielles en gestion de projets et en travail d'équipe. Je suis très enthousiaste à l'idée d'appliquer ces
                    compétences au domaine du développement informatique et de contribuer au succès de votre entreprise.
                </p>
            </div>
        </div>
    </div>
</section>