<!-- Section Témoignages -->
<section id="testimonials" class="testimonials section-bg py-5 fade-up-on-scroll">
    <!-- Conteneur principal -->
    <div class="container">
        <!-- Titre de la section -->
        <div class="section-title">
            <h2 class="h2 h1-md">Témoignage</h2>
        </div>
        <div class="testimonial mb-3">
            <img class="mb-3" src="/assets/img/Timothée.jpg" alt="">
            <h4>Timothée FICAT</h4>
            <h5>3types</h5>
            <h6>Tuteur de stage</h6>
            <p class="mt-3">
                <i class="bx bxs-quote-alt-left quote-icon-left quote"></i>
                Nicolas a su se rendre grandement utile au sein de mon entreprise durant son stage de formation.
                Il s'adapte vite à de nouvelles technologies et prend plaisir à apprendre.
                Indépendant et soucieux du travail bien fait, il est très investi dans son travail et sait prendre des initiatives.
                Je suis convaincu que Nicolas sera un atout précieux pour toute organisation et je le recommande vivement pour tout poste nécessitant compétence,
                leadership et initiative.
                <i class="bx bxs-quote-alt-right quote-icon-right quote"></i>
            </p>
        </div>
    </div>
</section>