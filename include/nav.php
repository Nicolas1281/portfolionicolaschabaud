<!-- En-tête de la page -->
<header id="header" class="d-flex flex-column justify-content-center">
    <!-- Menu de navigation pour les petits écrans -->
    <nav class="navbar position-fixed ms-3 d-lg-none position-absolute top-0 start-0 hamburger-menu">
        <div class="container-fluid">
            <!-- Bouton pour afficher le menu -->
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Contenu du menu -->
            <div class="offcanvas transparent-shadow-menu hamburger-width" tabindex="-1" id="offcanvasNavbar">
                <!-- En-tête du menu -->
                <div class="offcanvas-header">
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <!-- Corps du menu -->
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <!-- Élément Accueil -->
                        <li><a href="#hero" class="nav-link" aria-label="Accueil">
                                <!-- Icône pour Accueil -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.261 4.745a.375.375 0 0 0-.518 0l-8.63 8.244a.374.374 0 0 0-.115.271l-.002 7.737a1.5 1.5 0 0 0 1.5 1.5h4.505a.75.75 0 0 0 .75-.75v-6.375a.375.375 0 0 1 .375-.375h3.75a.375.375 0 0 1 .375.375v6.375a.75.75 0 0 0 .75.75h4.503a1.5 1.5 0 0 0 1.5-1.5V13.26a.374.374 0 0 0-.116-.271L12.26 4.745Z"></path>
                                        <path d="M23.011 11.444 19.505 8.09V3a.75.75 0 0 0-.75-.75h-2.25a.75.75 0 0 0-.75.75v1.5L13.04 1.904c-.254-.257-.632-.404-1.04-.404-.407 0-.784.147-1.038.405l-9.97 9.539a.765.765 0 0 0-.063 1.048.749.749 0 0 0 1.087.05l9.726-9.294a.375.375 0 0 1 .519 0l9.727 9.294a.75.75 0 0 0 1.059-.02c.288-.299.264-.791-.036-1.078Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour Accueil -->
                                <span class="hidden">Accueil</span></a>
                        </li>

                        <!-- Élément À propos -->
                        <li><a href="#about" class="nav-link scrollto" aria-label="A propos">
                                <!-- Icône pour À propos -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M15.592 3.027C14.68 2.042 13.406 1.5 12 1.5c-1.414 0-2.692.54-3.6 1.518-.918.99-1.365 2.334-1.26 3.786C7.348 9.67 9.528 12 12 12c2.472 0 4.648-2.33 4.86-5.195.106-1.439-.344-2.78-1.268-3.778Z"></path>
                                        <path d="M20.25 22.5H3.75a1.454 1.454 0 0 1-1.134-.522 1.655 1.655 0 0 1-.337-1.364c.396-2.195 1.63-4.038 3.571-5.333C7.574 14.132 9.758 13.5 12 13.5c2.242 0 4.426.633 6.15 1.781 1.94 1.294 3.176 3.138 3.571 5.332.091.503-.032 1-.336 1.365a1.453 1.453 0 0 1-1.135.522Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour À propos -->
                                <span class="hidden">À propos</span></a>
                        </li>

                        <!-- Élément CV -->
                        <li><a href="#resume" class="nav-link scrollto" aria-label="CV">
                                <!-- Icône pour CV -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M23.25 8.25a3.003 3.003 0 0 0-3-3H18V4.5a2.252 2.252 0 0 0-2.25-2.25h-7.5A2.252 2.252 0 0 0 6 4.5v.75H3.75a3.003 3.003 0 0 0-3 3v2.25h22.5V8.25Zm-6.75-3h-9V4.5a.75.75 0 0 1 .75-.75h7.5a.75.75 0 0 1 .75.75v.75Z"></path>
                                        <path d="M15.75 12.375a1.125 1.125 0 0 1-1.125 1.125h-5.25a1.125 1.125 0 0 1-1.125-1.125v-.188A.188.188 0 0 0 8.062 12H.75v6.75a3 3 0 0 0 3 3h16.5a3 3 0 0 0 3-3V12h-7.313a.188.188 0 0 0-.187.188v.187Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour CV -->
                                <span class="hidden">CV</span></a>
                        </li>

                        <!-- Élément Portfolio -->
                        <li><a href="#portfolio" class="nav-link scrollto" aria-label="portfolio">
                                <!-- Icône pour Portfolio -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13.594 1.5H6.75a3.003 3.003 0 0 0-3 3v15a3.003 3.003 0 0 0 3 3h6.844v-21Z"></path>
                                        <path d="M17.25 1.5h-.844v21h.844a3.004 3.004 0 0 0 3-3v-15a3.003 3.003 0 0 0-3-3Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour Portfolio -->
                                <span class="hidden">Portfolio</span></a>
                        </li>

                        <!-- Élément Veille Technologique -->
                        <li><a href="#veille" class="nav-link scrollto" aria-label="veille">
                                <!-- Icône pour Veille Technologique -->
                                <i>
                                    <svg width="25" height="25" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 3.75V3A2.257 2.257 0 0 1 8.25.75h10.5A2.257 2.257 0 0 1 21 3v17.25l-3.75-3"></path>
                                        <path d="M15 4.5H5.25A2.257 2.257 0 0 0 3 6.75v16.5l7.125-6 7.125 6V6.75A2.256 2.256 0 0 0 15 4.5Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour Veille Technologique -->
                                <span class="hidden">Veille Technologique</span></a>
                        </li>

                        <!-- Élément Témoignages -->
                        <li><a href="#testimonials" class="nav-link scrollto" aria-label="temoignage">
                                <!-- Icône pour Témoignages -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M18.469 22.5a.75.75 0 0 1-.44-.14L12 17.99l-6.029 4.37a.75.75 0 0 1-1.15-.847l2.35-6.965-6.093-4.178A.75.75 0 0 1 1.5 9h7.518l2.268-6.981a.75.75 0 0 1 1.427 0l2.27 6.984H22.5a.75.75 0 0 1 .424 1.369l-6.096 4.176 2.35 6.963a.75.75 0 0 1-.71.99Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour Témoignages -->
                                <span class="hidden">Témoignages</span></a>
                        </li>

                        <!-- Élément Contact -->
                        <li><a href="#contact" class="nav-link scrollto" aria-label="contact">
                                <!-- Icône pour Contact -->
                                <i>
                                    <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M19.875 3.75H4.125A2.628 2.628 0 0 0 1.5 6.375v11.25a2.628 2.628 0 0 0 2.625 2.625h15.75a2.627 2.627 0 0 0 2.625-2.625V6.375a2.627 2.627 0 0 0-2.625-2.625Zm-.665 4.342-6.75 5.25a.75.75 0 0 1-.92 0l-6.75-5.25a.75.75 0 1 1 .92-1.184L12 11.8l6.29-4.892a.75.75 0 0 1 .92 1.184Z"></path>
                                    </svg>
                                </i>
                                <!-- Texte caché pour Contact -->
                                <span class="hidden">Contact</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- Menu de navigation pour les grands écrans -->
    <nav id="navbar" class="navbar nav-menu position-fixed ms-3 d-none d-lg-block">
        <!-- Liste des éléments du menu -->
        <ul>
            <!-- Élément Accueil -->
            <li><a href="#hero" class="nav-link" aria-label="Accueil">
                    <!-- Icône pour Accueil -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.261 4.745a.375.375 0 0 0-.518 0l-8.63 8.244a.374.374 0 0 0-.115.271l-.002 7.737a1.5 1.5 0 0 0 1.5 1.5h4.505a.75.75 0 0 0 .75-.75v-6.375a.375.375 0 0 1 .375-.375h3.75a.375.375 0 0 1 .375.375v6.375a.75.75 0 0 0 .75.75h4.503a1.5 1.5 0 0 0 1.5-1.5V13.26a.374.374 0 0 0-.116-.271L12.26 4.745Z"></path>
                            <path d="M23.011 11.444 19.505 8.09V3a.75.75 0 0 0-.75-.75h-2.25a.75.75 0 0 0-.75.75v1.5L13.04 1.904c-.254-.257-.632-.404-1.04-.404-.407 0-.784.147-1.038.405l-9.97 9.539a.765.765 0 0 0-.063 1.048.749.749 0 0 0 1.087.05l9.726-9.294a.375.375 0 0 1 .519 0l9.727 9.294a.75.75 0 0 0 1.059-.02c.288-.299.264-.791-.036-1.078Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour Accueil -->
                    <span class="hidden">Accueil</span></a>
            </li>

            <!-- Élément À propos -->
            <li><a href="#about" class="nav-link scrollto" aria-label="A propos">
                    <!-- Icône pour À propos -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.592 3.027C14.68 2.042 13.406 1.5 12 1.5c-1.414 0-2.692.54-3.6 1.518-.918.99-1.365 2.334-1.26 3.786C7.348 9.67 9.528 12 12 12c2.472 0 4.648-2.33 4.86-5.195.106-1.439-.344-2.78-1.268-3.778Z"></path>
                            <path d="M20.25 22.5H3.75a1.454 1.454 0 0 1-1.134-.522 1.655 1.655 0 0 1-.337-1.364c.396-2.195 1.63-4.038 3.571-5.333C7.574 14.132 9.758 13.5 12 13.5c2.242 0 4.426.633 6.15 1.781 1.94 1.294 3.176 3.138 3.571 5.332.091.503-.032 1-.336 1.365a1.453 1.453 0 0 1-1.135.522Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour À propos -->
                    <span class="hidden">À propos</span></a>
            </li>

            <!-- Élément CV -->
            <li><a href="#resume" class="nav-link scrollto" aria-label="CV">
                    <!-- Icône pour CV -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.25 8.25a3.003 3.003 0 0 0-3-3H18V4.5a2.252 2.252 0 0 0-2.25-2.25h-7.5A2.252 2.252 0 0 0 6 4.5v.75H3.75a3.003 3.003 0 0 0-3 3v2.25h22.5V8.25Zm-6.75-3h-9V4.5a.75.75 0 0 1 .75-.75h7.5a.75.75 0 0 1 .75.75v.75Z"></path>
                            <path d="M15.75 12.375a1.125 1.125 0 0 1-1.125 1.125h-5.25a1.125 1.125 0 0 1-1.125-1.125v-.188A.188.188 0 0 0 8.062 12H.75v6.75a3 3 0 0 0 3 3h16.5a3 3 0 0 0 3-3V12h-7.313a.188.188 0 0 0-.187.188v.187Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour CV -->
                    <span class="hidden">CV</span></a>
            </li>

            <!-- Élément Portfolio -->
            <li><a href="#portfolio" class="nav-link scrollto" aria-label="portfolio">
                    <!-- Icône pour Portfolio -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M13.594 1.5H6.75a3.003 3.003 0 0 0-3 3v15a3.003 3.003 0 0 0 3 3h6.844v-21Z"></path>
                            <path d="M17.25 1.5h-.844v21h.844a3.004 3.004 0 0 0 3-3v-15a3.003 3.003 0 0 0-3-3Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour Portfolio -->
                    <span class="hidden">Portfolio</span></a>
            </li>

            <!-- Élément Veille Technologique -->
            <li><a href="#veille" class="nav-link scrollto" aria-label="veille">
                    <!-- Icône pour Veille Technologique -->
                    <i>
                        <svg width="25" height="25" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 3.75V3A2.257 2.257 0 0 1 8.25.75h10.5A2.257 2.257 0 0 1 21 3v17.25l-3.75-3"></path>
                            <path d="M15 4.5H5.25A2.257 2.257 0 0 0 3 6.75v16.5l7.125-6 7.125 6V6.75A2.256 2.256 0 0 0 15 4.5Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour Veille Technologique -->
                    <span class="hidden">Veille Technologique</span></a>
            </li>

            <!-- Élément Témoignages -->
            <li><a href="#testimonials" class="nav-link scrollto" aria-label="temoignage">
                    <!-- Icône pour Témoignages -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.469 22.5a.75.75 0 0 1-.44-.14L12 17.99l-6.029 4.37a.75.75 0 0 1-1.15-.847l2.35-6.965-6.093-4.178A.75.75 0 0 1 1.5 9h7.518l2.268-6.981a.75.75 0 0 1 1.427 0l2.27 6.984H22.5a.75.75 0 0 1 .424 1.369l-6.096 4.176 2.35 6.963a.75.75 0 0 1-.71.99Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour Témoignages -->
                    <span class="hidden">Témoignages</span></a>
            </li>

            <!-- Élément Contact -->
            <li><a href="#contact" class="nav-link scrollto" aria-label="contact">
                    <!-- Icône pour Contact -->
                    <i>
                        <svg width="25" height="25" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.875 3.75H4.125A2.628 2.628 0 0 0 1.5 6.375v11.25a2.628 2.628 0 0 0 2.625 2.625h15.75a2.627 2.627 0 0 0 2.625-2.625V6.375a2.627 2.627 0 0 0-2.625-2.625Zm-.665 4.342-6.75 5.25a.75.75 0 0 1-.92 0l-6.75-5.25a.75.75 0 1 1 .92-1.184L12 11.8l6.29-4.892a.75.75 0 0 1 .92 1.184Z"></path>
                        </svg>
                    </i>
                    <!-- Texte caché pour Contact -->
                    <span class="hidden">Contact</span></a>
            </li>
        </ul>
    </nav>