<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bienvenue sur le portfolio de Nicolas Chabaud, étudiant en BTS SIO, spécialisé en SLAM. Explorez mes projets, mes compétences en gestion de projet et mon parcours unique.">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/boxicons@2.1.1/css/boxicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <?php require_once('forms/contact.php'); ?>
    <link rel="icon" type="image/x-icon" href="/assets/img/favicon_io/favicon-16x16.png">
    <link rel="apple-touch-icon" href="/assets/img/favicon_io/apple-touch-icon.png">
    <title>Nicolas CHABAUD - Développeur Web et Mobile Passionné</title>
</head>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PDD58PWR48"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-PDD58PWR48');
</script>

<body>