<!-- Début du pied de page -->
<footer id="footer">
    <!-- Conteneur principal -->
    <div class="container custom-row justify-content-between border-top">
        <!-- Groupe 1 : Réseaux sociaux et informations de contact -->
        <div class="col-12 col-sm-4 col-md-3 d-flex position-relative mt-4 group-1">
            <!-- Conteneur pour les icônes et les informations -->
            <div class="d-flex flex-column align-items-center mb-5">
                <!-- Liste des icônes de réseaux sociaux -->
                <ul class="list-unstyled d-flex">
                    <!-- Facebook -->
                    <li class="icone"><a class="text-body-secondary" href="https://www.facebook.com/nico1201" aria-label="facebook"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                                <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z" />
                            </svg></a></li>
                    <!-- Twitter -->
                    <li class="icone ms-3"><a class="text-body-secondary" href="https://twitter.com/nicolas_chabaud" aria-label="twitter"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-twitter" viewBox="0 0 16 16">
                                <path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.286 3.286 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.286 3.286 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
                            </svg></a></li>
                    <!-- Instagram -->
                    <li class="icone ms-3"><a class="text-body-secondary" href="https://www.instagram.com/nico12_cbd/" aria-label="instagram"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-instagram" viewBox="0 0 16 16">
                                <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24-1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z" />
                            </svg></a></li>
                    <!-- LinkedIn -->
                    <li class="icone ms-3"><a class="text-body-secondary" href="https://www.linkedin.com/in/nicolas-chabaud-7533041aa/" aria-label="linkedin"><svg width="16" height="16" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M20.82 1.5H3.294c-.957 0-1.794.69-1.794 1.635v17.566c0 .951.837 1.799 1.794 1.799h17.521c.963 0 1.685-.854 1.685-1.8V3.136c.006-.946-.722-1.635-1.68-1.635ZM8.01 19.005H5V9.65h3.01v9.354ZM6.61 8.228h-.022c-.963 0-1.586-.716-1.586-1.613C5.002 5.7 5.642 5 6.626 5c.984 0 1.587.695 1.608 1.614 0 .897-.624 1.613-1.625 1.613Zm12.395 10.777h-3.009V13.89c0-1.225-.438-2.063-1.526-2.063-.832 0-1.324.563-1.543 1.111-.082.197-.104.465-.104.739v5.328H9.815V9.65h3.008v1.301c.438-.623 1.122-1.52 2.713-1.52 1.975 0 3.469 1.301 3.469 4.108v5.465Z"></path>
                            </svg></a></li>
                    <!-- Icône Gitlab -->
                    <li class="icone ms-3"><a class="text-body-secondary" href="https://gitlab.com/Nicolas1281" target="_blank" aria-label="linkedin"><svg width="16" height="16" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="m23.16 13.2-1.18-3.66a.511.511 0 0 0-.029-.098l-2.367-7.356a.941.941 0 0 0-1.45-.468.926.926 0 0 0-.337.473l-2.257 7.01H8.416l-2.259-7.01a.926.926 0 0 0-.884-.654h-.005a.945.945 0 0 0-.896.657l-2.37 7.363c0 .006-.005.012-.007.019L.793 13.2a1.366 1.366 0 0 0 .49 1.522l10.38 7.613a.527.527 0 0 0 .627-.003l10.382-7.61a1.365 1.365 0 0 0 .488-1.522ZM7.644 10.178l2.894 8.989-6.945-8.99h4.051Zm5.775 8.99 2.775-8.617.12-.375h4.055l-6.284 8.128-.666.864Zm5.275-16.386 2.033 6.32h-4.07l2.037-6.32Zm-3.498 7.392-2.016 6.261-1.203 3.73-3.214-9.991h6.432ZM5.262 2.78 7.3 9.1H3.235l2.028-6.32ZM1.907 13.856a.29.29 0 0 1-.103-.323l.89-2.766 6.545 8.465-7.332-5.376Zm20.14 0-7.332 5.374.025-.032 6.518-8.433.89 2.766a.29.29 0 0 1-.102.324"></path>
                            </svg></a></li>
                </ul>

                <!-- Adresse e-mail -->
                <h6 class="mt-2">Adresse mail</h6>
                <a href="mailto:nicolas.chab@hotmail.fr" class="interactive-effect">nicolas.chab@hotmail.fr</a>

                <!-- Numéro de téléphone -->
                <h6 class="mt-2">Contact</h6>
                <a href="tel:0640658435" class="interactive-effect">0640658435</a>

                <!-- Lien vers les mentions légales -->
                <p class="interactive-effect mention" id="project-image-12" data-toggle="modal" data-target="#project-modal-12">
                    Mentions légales - tout droit réservé
                </p>

                <!-- Modal pour les mentions légales -->
                <div class="modal fade" id="project-modal-12" tabindex="-1" role="dialog" aria-hidden="true">
                    <!-- Contenu du modal -->
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <!-- En-tête du modal -->
                            <div class="modal-header">
                                <h5 class="modal-title">Mentions légales</h5>
                                <button type="button" class="close close-modal-button text-blue" data-dismiss="modal" aria-label="Close" data-target="#project-modal-12">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <!-- Corps du modal -->
                            <div class="modal-body">
                                <!-- Contenu des mentions légales -->
                                <h1 class="text-center">Conditions générales d'utilisation et mentions légales</h1>
                                <h6 class="text-center">En vigueur au 16/09/2023</h6>
                                <h3 class="mt-5">Introduction</h3>
                                <p>
                                    Merci de lire attentivement les modalités d'utilisation du présent site avant de le parcourir. En vous rendant sur ce site,
                                    vous acceptez sans réserve les présentes modalités.
                                </p>
                                <h3>Éditeur du site</h3>
                                <p>
                                    Nicolas CHABAUD <br>
                                    Email : nicolas.chab@hotmail.fr
                                </p>
                                <h3>Conditions d'utilisation</h3>
                                <p>
                                    Le site accessible par l'URL https://www.nicolaschabaud.com est exploité dans le respect de la législation française. L'utilisation de
                                    ce site est régie par les présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance de ces
                                    conditions et les avoir acceptées. Celles-ci pourront être modifiées à tout moment et sans préavis.
                                </p>
                                <h3>Article 1 : les mentions légales</h3>
                                <p>
                                    L’édition et la direction de la publication du site https://www.nicolaschabaud.com est assurée par Nicolas CHABAUD, domicilié 4
                                    boulevard Joliot Curie, 13250 Saint-chamas.
                                </p>
                                <ul>
                                    <li><strong>Numéro de téléphone</strong> : 0640658435</li>
                                    <li><strong>Adresse e-mail</strong> : nicolas.chab@hotmail.fr</li>
                                </ul>
                                <p>
                                    L'hébergeur du site https://www.nicolaschabaud.com est la société Ionos, dont le siège social est situé au 7 Pl. de la Gare, 57200
                                    Sarreguemines, avec le numéro de téléphone : 09 70 80 89 11.
                                </p>
                                <h3>Article 2 : Accès au site</h3>
                                <p>
                                    Le site est accessible gratuitement en tout lieu à tout utilisateur ayant un accès à Internet. Tous les frais supportés par
                                    l'utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
                                </p>
                                <h3>Article 3 : Collecte des données</h3>
                                <p>
                                    Le site assure à l'utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément
                                    à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.
                                    L'utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'utilisateur
                                    exerce ce droit :
                                </p>
                                <ul>
                                    <li>par mail à l'adresse email nicolas.chab@hotmail.fr</li>
                                    <li>via un formulaire de contact</li>
                                </ul>
                                <h3>Article 4 : propriété intellectuelle</h3>
                                <p>
                                    Les marques, logos, signes ainsi que tous les contenus du site (textes, images, sons…) font l'objet d'une protection par le Code de la
                                    propriété intellectuelle et plus particulièrement par le droit d'auteur.
                                    L'utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus.
                                    Il s'engage à une utilisation des contenus du site dans un cadre strictement privé. Toute utilisation à des fins commerciales et
                                    publicitaires est strictement interdite.
                                </p>
                                <h3>Article 5 : Responsabilité</h3>
                                <p>
                                    Les sources des informations diffusées sur le site https://www.nicolaschabaud.com sont réputées fiables. Toutefois, le site se réserve
                                    la faculté d'une non-garantie de la fiabilité des sources. Les informations données sur le site le sont à titre purely informatif.
                                    Ainsi, l'utilisateur assume seul l'entière responsabilité de l'utilisation des informations et contenus du présent site.
                                </p>
                                <h3>Article 6 : Liens hypertextes</h3>
                                <p>
                                    Des liens hypertextes peuvent être présents sur le site. L'utilisateur est informé qu’en cliquant sur ces liens, il sortira du site
                                    https://www.nicolaschabaud.com. Ce dernier n’a pas de contrôle sur les pages Web sur lesquelles aboutissent ces liens et ne saurait,
                                    en aucun cas, être responsable de leur contenu.
                                </p>
                                <h3>Article 7 : Cookies</h3>
                                <p>
                                    L'utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de
                                    navigation. Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’utilisateur par votre
                                    navigateur et qui sont nécessaires à l’utilisation du site https://www.nicolaschabaud.com. Les cookies ne contiennent pas d’information
                                    personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement
                                    et donc anonyme. Certains cookies expirent à la fin de la visite de l’utilisateur, d’autres restent.
                                </p>
                                <h3>Article 8 : Droit applicable et juridiction compétente</h3>
                                <p>
                                    La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, les
                                    tribunaux français seront seuls compétents pour en connaître.
                                </p>
                                <h3>Crédits</h3>
                                <ul>
                                    <li>
                                        <strong>Icons8</strong> : certaines icônes sont fournies par Icons8 et sont utilisées conformément à leur
                                        <a href="https://icons8.com/license" target="_blank">licence</a>.<br>
                                        <a target="_blank" href="https://icons8.com/icon/v9uZbuVoWleB/wordpress">WordPress</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/zfHRZ6i1Wg0U/figma">Figma</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/34886/gitlab">Gitlab</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/UFXRpPFebwa2/logo-de-mysql">mysql</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/7I3BjCqe9rjG/flutter">Flutter</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/78295/symfony">Symfony</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/wPohyHO_qO1a/r%C3%A9agir">Réagir</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/EzPCiQUqWWEa/bootstrap">Bootstrap</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/7AFcZ2zirX6Y/dart">Dart</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/fAMVO_fuoOuC/logo-php">php</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/108784/javascript">Javascript</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/21278/css3">css</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/v8RpPQUwv0N8/html-5">html</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a><br>
                                        <a target="_blank" href="https://icons8.com/icon/12599/github">GitHub</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
                                        Site web : <a href="https://icons8.com" target="_blank">Icons8</a>
                                    </li>
                                </ul>
                                <h3>Ressources de stage</h3>
                                <p>La plupart des ressources images et vidéo utilisées lors de mon stage appartiennent à
                                    <a href="https://3types.fr/" target="_blank">3Types</a>.
                                </p>
                                <h3>Contactez-nous</h3>
                                <p>Pour toute demande d'autorisation ou d'information, veuillez nous contacter par email : nicolas.chab@hotmail.fr</p>
                            </div>
                            <!-- Pied de page du modal -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary close-modal-button text-blue" data-dismiss="modal" data-target="#project-modal-12">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Groupe 2 : Copyright -->
        <div class="col-12 col-sm-4 d-flex position-relative mt-2 group-2">
            <p>© Copyright 2023 - Nicolas CHABAUD</p>
        </div>

        <!-- Groupe 3 : Crédits du site web -->
        <div class="col-12 col-sm-4 col-md-3 d-flex position-relative group-3">
            <p>Site web : Nicolas CHABAUD</p>
        </div>
    </div>
</footer>
<!-- Fin du pied de page -->
</body>
<script src="script.js"></script>

</html>