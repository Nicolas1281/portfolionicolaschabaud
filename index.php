<?php include('include/head.php'); ?>
<?php include('include/nav.php'); ?>
<?php include('include/header.php'); ?>

<?php include('section/about.php'); ?>
<?php include('section/facts.php'); ?>
<?php include('section/resume.php'); ?>
<?php include('section/skills.php'); ?>
<?php include('section/portfolio.php'); ?>
<?php include('section/veille.php'); ?>
<?php include('section/testimonials.php'); ?>
<?php include('section/contact.php'); ?>

<?php include('include/footer.php'); ?>