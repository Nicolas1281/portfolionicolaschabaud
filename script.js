const words = ["web", "mobile", "logiciel"];
let index = 0;
let wordIndex = 0;
let currentWord = "";
let isDeleting = false;

function type() {
  const typedTextSpan = document.getElementById("typed-text");
  const cursorSpan = document.getElementById("cursor");

  if (isDeleting && currentWord.length > 0) {
    currentWord = currentWord.substring(0, currentWord.length - 1);
  } else if (!isDeleting && wordIndex < words[index].length) {
    currentWord += words[index][wordIndex];
    wordIndex++;
  }

  if (currentWord.length === words[index].length) {
    isDeleting = true;
  }

  if (isDeleting && currentWord.length === 0) {
    isDeleting = false;
    index = (index + 1) % words.length;
    wordIndex = 0;
  }

  typedTextSpan.textContent = currentWord;
  setTimeout(type, isDeleting ? 500 : 200);
}

document.addEventListener("DOMContentLoaded", function () {
  setTimeout(type, 1000);
});

let navbar = document.getElementById("navbar");
let timeout;

// Fonction pour rendre la barre de navigation transparente
function makeTransparent() {
  navbar.classList.add("transparent");
}

// Fonction pour rendre la barre de navigation opaque
function makeOpaque() {
  navbar.classList.remove("transparent");
}

// Événement lorsque la souris passe sur la barre de navigation
navbar.addEventListener("mouseover", function () {
  clearTimeout(timeout); // Annule le délai s'il est en cours
  makeOpaque();
});

// Événement lorsque la souris quitte la barre de navigation
navbar.addEventListener("mouseout", function () {
  timeout = setTimeout(makeTransparent, 2000); // Met un délai de 2 secondes
});

// Met un délai initial de 2 secondes pour rendre la barre de navigation transparente
timeout = setTimeout(makeTransparent, 2000);

$(document).ready(function () {
  $(window).on("scroll", function () {
    var scrollPosition = $(this).scrollTop();

    // Ajoutez la moitié de la hauteur de la fenêtre à la valeur offset pour une détection plus précoce
    var windowHeight = $(window).height();

    if (
      scrollPosition >= $("#hero").offset().top - windowHeight / 2 &&
      scrollPosition < $("#about").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#hero"]').addClass("active");
    } else if (
      scrollPosition >= $("#about").offset().top - windowHeight / 2 &&
      scrollPosition < $("#resume").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#about"]').addClass("active");
    } else if (
      scrollPosition >= $("#resume").offset().top - windowHeight / 2 &&
      scrollPosition < $("#portfolio").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#resume"]').addClass("active");
    } else if (
      scrollPosition >= $("#portfolio").offset().top - windowHeight / 2 &&
      scrollPosition < $("#veille").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#portfolio"]').addClass("active");
    } else if (
      scrollPosition >= $("#veille").offset().top - windowHeight / 2 &&
      scrollPosition < $("#testimonials").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#veille"]').addClass("active");
    } else if (
      scrollPosition >= $("#testimonials").offset().top - windowHeight / 2 &&
      scrollPosition < $("#contact").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#testimonials"]').addClass("active");
    } else if (
      scrollPosition >=
      $("#contact").offset().top - windowHeight / 2
    ) {
      $(".nav-link").removeClass("active");
      $('.nav-link[href="#contact"]').addClass("active");
    }
  });
});

window.addEventListener("DOMContentLoaded", function () {
  const progressBars = document.querySelectorAll(".progress-bar");

  progressBars.forEach((bar) => {
    const value = parseInt(bar.getAttribute("data-value"));
    let bgColor;
    let textColor = "white"; // Default text color

    if (value < 30) {
      bgColor = "red";
    } else if (value < 70) {
      bgColor = "#E1D304";
      textColor = "black"; // Change text color to black for yellow background
    } else {
      bgColor = "#03A500";
    }

    bar.style.backgroundColor = bgColor;
    bar.style.color = textColor;
  });
});

// Fonction pour vérifier si un élément est dans le viewport
function isInViewport(element) {
  const rect = element.getBoundingClientRect();
  return (
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.bottom >= 0
  );
}

// Fonction pour activer les animations lors du défilement
function checkScroll() {
  const zoomInElements = document.querySelectorAll(
    ".zoom-in-on-scroll:not(.active)"
  );
  const fadeUpElements = document.querySelectorAll(
    ".fade-up-on-scroll:not(.active)"
  );
  const counters = document.querySelectorAll(
    ".purecounter:not([data-animated])"
  );

  // Pour les éléments Zoom In
  zoomInElements.forEach((element) => {
    if (isInViewport(element)) {
      element.classList.add("active");
    }
  });

  // Pour les éléments Fade Up
  fadeUpElements.forEach((element) => {
    if (isInViewport(element)) {
      element.classList.add("active");
    }
  });

  // Fonction pour le compteur animé
  function smoothCounter(counter, target, duration) {
    let startTime = null;

    function animate(currentTime) {
      if (startTime === null) startTime = currentTime;
      const timeElapsed = currentTime - startTime;

      const progress = Math.min(timeElapsed / duration, 1);
      counter.textContent = Math.floor(progress * target);

      if (timeElapsed < duration) {
        requestAnimationFrame(animate);
      } else {
        counter.textContent = target;
        counter.setAttribute("data-animated", "true");
      }
    }

    requestAnimationFrame(animate);
  }

  // Pour les compteurs
  counters.forEach((counter) => {
    if (isInViewport(counter)) {
      const target = +counter.getAttribute("data-purecounter-end");
      const duration = +counter.getAttribute("data-purecounter-duration");
      smoothCounter(counter, target, duration);
    }
  });
}

// Événement de défilement
window.addEventListener("scroll", checkScroll);

// Vérifier au chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  setTimeout(checkScroll, 100); // Ajout d'un délai pour s'assurer que tous les éléments sont chargés
});

$(document).ready(function () {
  // Lorsqu'un bouton de filtre est cliqué
  $(".filter-button").click(function () {
    var value = $(this).attr("data-filter");

    // Afficher/masquer les projets en fonction du filtre sélectionné
    if (value == "*") {
      $(".portfolio-item").show("1000");
    } else {
      $(".portfolio-item").hide("1000").filter(value).show("1000");
    }

    // Mettre en surbrillance le bouton actif
    $(".filter-button").removeClass("active");
    $(this).addClass("active");
  });
});

// Sélectionnez les images de projet en utilisant leurs ID individuellement
for (var i = 1; i <= 12; i++) {
  var image = document.getElementById("project-image-" + i);
  var modal = document.getElementById("project-modal-" + i);

  // Ajoutez un gestionnaire d'événements de clic à l'image
  image.addEventListener(
    "click",
    (function (currentModal) {
      return function () {
        // Ouvrez le modal
        $(currentModal).modal("show");
      };
    })(modal)
  );
}

// Sélectionnez les video de projet en utilisant leurs ID individuellement
for (var i = 1; i <= 8; i++) {
  var image = document.getElementById("project-video-" + i);
  var modal = document.getElementById("project-modal-video-" + i);

  // Ajoutez un gestionnaire d'événements de clic à l'image
  image.addEventListener(
    "click",
    (function (currentModal) {
      return function () {
        // Ouvrez le modal
        $(currentModal).modal("show");
      };
    })(modal)
  );
}

// Sélectionnez les boutons de fermeture du modal
var closeModalButtons = document.querySelectorAll(".close-modal-button");

// Ajoutez un gestionnaire d'événements de clic à chaque bouton de fermeture
closeModalButtons.forEach(function (button) {
  button.addEventListener("click", function () {
    // Récupérez l'ID du modal à fermer à partir de l'attribut data-target
    var targetModalId = button.getAttribute("data-target");

    // Fermez le modal correspondant en utilisant jQuery
    $(targetModalId).modal("hide");
  });
});

document.addEventListener("DOMContentLoaded", function () {
  document
    .getElementById("contact-form")
    .addEventListener("submit", function (event) {
      event.preventDefault(); // Empêche la soumission du formulaire par défaut

      // Créez un objet FormData pour collecter les données du formulaire
      var formData = new FormData(this);

      // Effectuez une requête AJAX vers votre script PHP de traitement
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/forms/contact.php", true);

      // Gestionnaire d'événements lorsque la requête est terminée
      xhr.onload = function () {
        if (xhr.status === 200) {
          // Réponse du serveur (contient les messages d'erreur ou de succès)
          var response = JSON.parse(xhr.responseText);

          // Affichez les messages dans la zone appropriée
          var formMessages = document.getElementById("form-messages");
          formMessages.innerHTML = ""; // Effacez les messages précédents

          if (response.success) {
            formMessages.innerHTML =
              '<div class="alert alert-success">' + response.success + "</div>";
          } else if (response.error) {
            formMessages.innerHTML =
              '<div class="alert alert-danger">' + response.error + "</div>";
          }
        }
      };

      // Envoyez les données du formulaire au serveur
      xhr.send(formData);
    });
});

document.addEventListener("DOMContentLoaded", function () {
  const form = document.getElementById("contact-form");

  form.addEventListener("submit", function (event) {
    event.preventDefault(); // Empêche l'envoi du formulaire

    const name = document.getElementById("name");
    const email = document.getElementById("email");
    const subject = document.getElementById("subject");
    const message = document.getElementById("message");

    let errors = [];

    if (!name.value) {
      errors.push("Le champ 'Nom' est requis.");
    }

    if (!email.value) {
      errors.push("Le champ 'Adresse e-mail' est requis.");
    } else if (!email.value.includes("@")) {
      errors.push("L'adresse e-mail n'est pas valide.");
    }

    if (!subject.value) {
      errors.push("Le champ 'Sujet' est requis.");
    }

    if (!message.value) {
      errors.push("Le champ 'Message' est requis.");
    }

    const errorMessageDiv = document.getElementById("error-message");
    const successMessageDiv = document.getElementById("success-message");

    if (errors.length > 0) {
      errorMessageDiv.innerHTML = errors.join("<br>");
      errorMessageDiv.style.display = "block";
      successMessageDiv.style.display = "none";
    } else {
      successMessageDiv.innerHTML = "Le message a été envoyé avec succès.";
      successMessageDiv.style.display = "block";
      errorMessageDiv.style.display = "none";

      // Vider tous les champs du formulaire
      name.value = "";
      email.value = "";
      subject.value = "";
      message.value = "";
    }
  });
});
