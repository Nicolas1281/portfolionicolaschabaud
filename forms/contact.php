<?php
$errors = []; // Initialisez la variable $errors comme un tableau vide
$successMessage = ""; // Initialisez la variable $successMessage comme une chaîne vide

if ($_SERVER["REQUEST_METHOD"] === "POST") {
  // Validation des champs

  if (empty($_POST["name"])) {
    $errors[] = "Le champ 'Nom' est requis.";
  } else {
    $name = htmlspecialchars($_POST["name"]);
  }

  if (empty($_POST["email"])) {
    $errors[] = "Le champ 'Adresse e-mail' est requis.";
  } elseif (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
    $errors[] = "L'adresse e-mail n'est pas valide.";
  } else {
    $email = htmlspecialchars($_POST["email"]);
  }

  if (empty($_POST["subject"])) {
    $errors[] = "Le champ 'Sujet' est requis.";
  } else {
    $subject = htmlspecialchars($_POST["subject"]);
  }

  if (empty($_POST["message"])) {
    $errors[] = "Le champ 'Message' est requis.";
  } else {
    $message = htmlspecialchars($_POST["message"]);
  }

  if (empty($errors)) {
    // Configuration du serveur SMTP (IONOS)
    ini_set("SMTP", "smtp.ionos.fr");  // Serveur sortant (SMTP)
    ini_set("smtp_port", 465);         // Port sortant (TLS doit être activé)
    ini_set("username", "contact@nicolaschabaud.com");  // Nom d'utilisateur

    // Envoyer un message de confirmation
    $to = "contact@nicolaschabaud.com";
    $headers = "From: " . $email;

    if (mail($to, $subject, $message, $headers)) {
      // Message de succès
      $successMessage = "Le message a été envoyé avec succès.";
    } else {
      // Message d'erreur
      $errors[] = "Erreur lors de l'envoi du message.";
    }
  }
}
